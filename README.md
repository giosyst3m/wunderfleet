# Wizard 

![](http://giosyst3m.net/wp-content/uploads/2018/10/giosyst3m-logo-white-digital-color.png)

[![](https://img.shields.io/badge/tag-v.1.0.0-blue.svg)](https://bitbucket.org/giosyst3m/wunderfleet)

**Table of Contents**

[TOC]

# Introducction
It is a wizard where you get and save some information from your use.

# Wireframe
It has been thinking mobil first

![](http://giosyst3m.net/wp-content/uploads/2018/10/giosyst3m-Under-Wireframes-steps.png)

## Step 1: Personal Information
1. Save name, lastname, phone and step as 1 in the table USER.
2. Return ID user Encrypt.
3. Create Cookie with ID Encrypt. 

## Step 2: Address Information.
1. Save Street, house number, zip code and city, relate tu ID
2. Update Step on table

## Step 3: Account Information.
1. Save Account Owner and IBAN
2. Send information API and get paymentDataId.
3. Update Payment in table.

## Step 4: Seccuess and Finish.
1. Show user result 2xx Confirm
2. Show user error 4xx and tray again.

# Question & Answer
## Describe possible performance optimizations for your code
For increase the performance it should use:

1. Varnish Cached to improve frontend
2. Memcached to improve database query.
3. Active SSL to give a user Secure Web App.


- Use libraries which can help to Minify JavaScript and CCS files.
- I would be nice to have Backend with a REST API to admin whole request and send to Frontend, and I have Frontend develop on JavaScript Framework. That help a lot to the perfomance and wolud be a scale and modular Web APP.

## Whitch things could be done better, that you've done it?

1. I could use Promises JavaScript to help get best control each request, 
2. Using frameworks JavaScrit such as Angular, React or Ember which help to develop easy and clean,
3. On other hands I will using NodeJS as backend and
4. database no-sql such as MongoDB, Firebase,
5. Send a email to user to confirm to user.
 
## Why Usu Yii Framework

It is a excelente Framework which works MVC PHP web application, I have using meny programa, to help to develop app as fast as I can, It has a ORM which is very helpfull to conect with diferent database, and help to create Query easy.

I acutally I have a web app with my client it's about Order Administrador for Small companies. click [http://ordenes.giosyst3m.net/](http://ordenes.giosyst3m.net/)

Working woth framework works with a base and help to develop many things and integrate each module.

# Demo
You can see themo this link [Demo](http://wunderfleet.giosyst3m.net/ "Demo") 

# Cookie
The Web APP create a Cookie and save User ID Encrypt MD5 plus a SEED, if user left App and get back, app checks is the cookie exists and get information and fill wholl filed with user save and your last visit.
Then leave last step was saved.
Name Cookie is **w_id**

# Flow Chart
As you can see this a diagram to explaint how information is working.

![](http://giosyst3m.net/wp-content/uploads/2018/10/giosyst3m-wunder-flow-chart.png)

# Setup

## Clone Repository Giosyst3m
You can clone 
`git clone https://giosyst3m@bitbucket.org/giosyst3m/wunderfleet.git`
or
`git clone git@bitbucket.org:giosyst3m/wunderfleet.git`

## Database Estructure
As you can see this how interactive user with database.

![](http://giosyst3m.net/wp-content/uploads/2018/10/giosyst3m-wunder-database-estructure.png)

## Database MySQL

You have to create a databese and execute this sql script to create USER table

```sql
/*
 Navicat Premium Data Transfer

 Source Server         : localhost-root
 Source Server Type    : MySQL
 Source Server Version : 50538
 Source Host           : localhost
 Source Database       : wunder

 Target Server Type    : MySQL
 Target Server Version : 50538
 File Encoding         : utf-8

 Date: 10/04/2018 07:37:45 AM
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `_id` varchar(50) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `step` int(11) NOT NULL DEFAULT '1',
  `address` varchar(25) DEFAULT NULL,
  `additional` varchar(15) DEFAULT NULL,
  `zip` varchar(25) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `account` varchar(50) DEFAULT NULL,
  `iban` varchar(15) DEFAULT NULL,
  `payment` varchar(500) DEFAULT NULL,
  `r_c_d` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `r_d_s` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=424 DEFAULT CHARSET=utf8;

SET FOREIGN_KEY_CHECKS = 1;
```
## Config Database in APP
You have to modify files as your Envarioment
### Live
`/protected/config/db-liv.php`
### Develop
`/protected/config/db-dev.php`

```php
<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


return array(
    'class' => 'CDbConnection',
    'connectionString' => 'mysql:host=localhost;dbname=wunder', // Put your database name
    'emulatePrepare' => true,
    'username' => 'wunder_user', // Put your user name
    'password' => 'wundoer01@', // Put your password 
    'charset' => 'utf8',
    'initSQLs' => array(
        "SET time_zone = '-5:00'", // Set your Time Zone
    )
);
```
# Library
## Yii Framework
Yii is an open source, object-oriented, component-based MVC PHP web application framework. Yii is pronounced as "Yee" or [ji:] and in Chinese it means "simple and evolutionary" and it can be an acronym for "Yes It Is!".
Link [https://www.yiiframework.com/](https://www.yiiframework.com/)

## Bootstrap
Bootstrap is a free and open-source front-end framework for designing websites and web applications. It contains HTML- and CSS-based design templates for typography, forms, buttons, navigation and other interface components, as well as optional JavaScript extensions
Link [http://getbootstrap.com/](http://getbootstrap.com/)

## JQUERY
Query is a cross-platform JavaScript library designed to simplify the client-side scripting of HTML. It is free, open-source software using the permissive MIT License.
link [https://jquery.com/](https://jquery.com/)

## Theme 
Gentelella Admin is a free to use Bootstrap admin template. This template uses the default Bootstrap 3 styles along with a variety of powerful jQuery plugins and tools to create a powerful framework for creating admin panels or back-end dashboards.
Link [https://github.com/puikinsh/gentelella](https://github.com/puikinsh/gentelella)
