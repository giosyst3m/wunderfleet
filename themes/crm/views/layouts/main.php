<!DOCTYPE html>
<html lang="en">
    <head>
        <?php if (ENV == 'liv') { ?>
            <!-- Google Tag Manager -->
            <script>(function (w, d, s, l, i) {
                    w[l] = w[l] || [];
                    w[l].push({'gtm.start':
                                new Date().getTime(), event: 'gtm.js'});
                    var f = d.getElementsByTagName(s)[0],
                            j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
                    j.async = true;
                    j.src =
                            'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                    f.parentNode.insertBefore(j, f);
                })(window, document, 'script', 'dataLayer', 'GTM-WX87Z9');</script>
            <!-- End Google Tag Manager -->
        <?php } else { ?>
            <!-- Google Tag Manager -->
            <script>(function (w, d, s, l, i) {
                    w[l] = w[l] || [];
                    w[l].push({'gtm.start':
                                new Date().getTime(), event: 'gtm.js'});
                    var f = d.getElementsByTagName(s)[0],
                            j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
                    j.async = true;
                    j.src =
                            'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                    f.parentNode.insertBefore(j, f);
                })(window, document, 'script', 'dataLayer', 'GTM-5TXSL5');</script>
            <!-- End Google Tag Manager -->
        <?php } ?>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
        <?php
        $cs = Yii::app()->clientScript;
        $themePath = Yii::app()->theme->baseUrl;

        /**
         * StyleSHeets
         */
        $cs
                ->registerCssFile($themePath . '/css/jquery-ui-1.8.19.custom.css')
                ->registerCssFile($themePath . '/css/dashboard/animate.css')
                ->registerCssFile($themePath . '/css/bootstrap-select.css')
                ->registerCssFile($themePath . '/css/bootstrap-timepicker.css')
                ->registerCssFile($themePath . '/css/bootstrap-datetimepicker.css')
                ->registerCssFile($themePath . '/css/ui.jqgrid.css')
                ->registerCssFile($themePath . '/js/jquery.fancybox/jquery.fancybox.css?v=2.1.5')
                ->registerCssFile($themePath . '/js/jquery.fancybox/helpers/jquery.fancybox-buttons.css?v=1.0.5')
                ->registerCssFile($themePath . '/js/jquery.fancybox/helpers/jquery.fancybox-thumbs.css?v=1.0.7')
                ->registerCssFile($themePath . '/css/colors.css')
                ->registerCssFile($themePath . '/css/program.css')
                ->registerCssFile($themePath . '/css/core.css');
        /**
         * JavaScripts
         */
        $cs
                ->registerCoreScript('jquery', CClientScript::POS_END)
                ->registerCoreScript('jquery.ui', CClientScript::POS_END)
                ->registerScriptFile($themePath . '/js/bootstrap.min.js', CClientScript::POS_END)
                ->registerScriptFile($themePath . '/js/dashboard/bootstrap-growl.js', CClientScript::POS_END)
                ->registerScriptFile($themePath . '/js/moment.js', CClientScript::POS_END)
                ->registerScriptFile($themePath . '/js/bootstrap-select.js', CClientScript::POS_END)
                ->registerScriptFile($themePath . '/js/bootstrap-timepicker.js', CClientScript::POS_END)
                ->registerScriptFile($themePath . '/js/bootstrap-datetimepicker.min.js', CClientScript::POS_END)
                ->registerScriptFile($themePath . '/js/grid.locale-es.js', CClientScript::POS_END)
                ->registerScriptFile($themePath . '/js/jquery.jqGrid.min.js', CClientScript::POS_END)
                ->registerScriptFile($themePath . '/vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js', CClientScript::POS_END)
                ->registerScriptFile($themePath . '/vendors/jquery.hotkeys/jquery.hotkeys.js', CClientScript::POS_END)
                ->registerScriptFile($themePath . '/vendors/google-code-prettify/src/prettify.js', CClientScript::POS_END)
                ->registerScriptFile($themePath . '/js/jquery.mousewheel-3.0.6.pack.js', CClientScript::POS_END)
                ->registerScriptFile($themePath . '/js/jquery.fancybox/jquery.fancybox.js?v=2.1.5', CClientScript::POS_END)
                ->registerScriptFile($themePath . '/js/jquery.fancybox/helpers/jquery.fancybox-buttons.js?v=1.0.5', CClientScript::POS_END)
                ->registerScriptFile($themePath . '/js/jquery.fancybox/helpers/jquery.fancybox-thumbs.js?v=1.0.7', CClientScript::POS_END)
                ->registerScriptFile($themePath . '/js/jquery.fancybox/helpers/jquery.fancybox-media.js?v=1.0.6', CClientScript::POS_END)
                ->registerScriptFile($themePath . '/script/core.js', CClientScript::POS_END)
                ->registerScript('tooltip', "$('[data-toggle=\"tooltip\"]').tooltip();
            $('[data-toggle=\"popover\"]').tooltip()"
                        , CClientScript::POS_READY);
        $detect = Yii::app()->mobileDetect;
        ?>
        <!-- Bootstrap -->
        <link href="<?php echo $themePath; ?>/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="<?php echo $themePath; ?>/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- NProgress -->
        <link href="<?php echo $themePath; ?>/vendors/nprogress/nprogress.css" rel="stylesheet">

        <!-- Custom Theme Style -->
        <link href="<?php echo $themePath; ?>/build/css/custom.min.css" rel="stylesheet">
        <link href='https://fonts.googleapis.com/css?family=Questrial' rel='stylesheet' type='text/css'>
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <script type="text/javascript" src="https://www.google.com/jsapi"></script>
        <!-- Bootstrap Colorpicker -->
        <link href="<?php echo $themePath; ?>/vendors/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css" rel="stylesheet">
    </head>

    <body class="<?php echo ($detect->isMobile() || $detect->isTablet() ? 'nav-md' : 'nav-sm'); ?>">
        <?php if (ENV == 'liv') { ?>
            <!-- Google Tag Manager (noscript) -->
            <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WX87Z9"
                              height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
            <!-- End Google Tag Manager (noscript) -->
        <?php } else { ?>
            <!-- Google Tag Manager (noscript) -->
            <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5TXSL5"
                              height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
            <!-- End Google Tag Manager (noscript) -->
        <?php } ?>
        <input type="hidden" value="<?php echo Yii::app()->homeUrl ?>/" id="homeUrl">  
        <div class="container body">
            <div class="main_container">
                <div class="col-md-3 left_col menu_fixed">
                    <div class="left_col scroll-view">
                        <div class="navbar nav_title" style="border: 0;">
                            <a href="<?php echo Yii::app()->createUrl('site'); ?>" class="site_title"><i class="fa fa-home"></i> <span>Wunder Fleet</span></a>
                        </div>

                        <div class="clearfix"></div>

                        <!-- menu profile quick info -->
                        <div class="profile">
                            <div class="profile_pic">
                                <img src="<?php echo $themePath ?>/images/guess.jpg" alt="Guess Picture" class="img-circle profile_img">
                            </div>
                            <div class="profile_info">
                                <span><?php echo Yii::t('app', 'Welcome'); ?></span>
                                <h2 id="user_menu">Guess</h2>
                            </div>
                        </div>
                        <!-- /menu profile quick info -->

                        <br />


                        <!-- sidebar menu -->

                        <!-- /sidebar menu -->
                    </div>
                </div>

                <!-- top navigation -->
                <div class="top_nav navbar-fixed-top">
                    <div class="nav_menu">
                        <nav>
                            <div class="nav toggle">
                                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                            </div>

                            <ul class="nav navbar-nav navbar-right">
                                <li class="">
                                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        <img src="<?php echo $themePath ?>/images/guess.jpg"  alt=""><span>Guess</span>
                                        <span class=" fa fa-angle-down"></span>
                                    </a>
                                    <ul class="dropdown-menu dropdown-usermenu pull-right">
                                        <li><a href="<?php echo Yii::app()->createUrl('/sistema/help/version/n/1.0.0'); ?>"><i class="fa fa-question pull-right"></i>help</a></li>
                                    </ul>
                                </li>
                                <li style="margin-top:10px;">
                                </li>  
                            </ul>
                        </nav>
                    </div>
                </div>
                <!-- /top navigation -->
                <br>
                <br>
                <br>
                <br>
                <!-- page content -->
                <div class="right_col" role="main">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <?php if (isset($this->breadcrumbs)): ?>

                                <?php
                                $this->widget('zii.widgets.CBreadcrumbs', array(
                                    'links' => $this->breadcrumbs,
                                ));
                                ?><!-- breadcrumbs -->
                            <?php endif ?>
                            <?php
                            $flashMessages = Yii::app()->user->getFlashes();
                            if ($flashMessages) {
                                foreach ($flashMessages as $key => $message) {
                                    echo BsHtml::tag('div', array('class' => 'info'), BsHtml::alert('alert alert-' . $key, $message), true);
                                }
                            }
                            if (!Yii::app()->user->isGuest) {
                                if (Yii::app()->user->getState('SYSTEM')->SYSTEM_MAINTENANCE_ACTIVE == 'true') {
                                    echo BsHtml::tag('div', ['class' => 'alert alert-dismissible ' . Yii::app()->user->getState('SYSTEM')->SYSTEM_MAINTENANCE_MESSAGE_COLOR . ' in fade', 'style' => '    border: 1px solid !important;'], '<a href="#" class="close" data-dismiss="alert" type="button">×</a>' . Yii::app()->user->getState('SYSTEM')->SYSTEM_MAINTENANCE_MESSAGE);
                                }
                            }
                            ?>
                            <?php echo $content; ?>
                        </div>
                    </div>
                </div>
                <!-- /page content -->

                <!-- footer content -->
                <footer>
                    <span id="top-link-block" class="hidden">
                        <a href="#top" class="btn btn-orange"  onclick="$('html,body').animate({scrollTop: 0}, 'slow');
                return false;">
                            <i class="glyphicon glyphicon-chevron-up"></i> Ir al inicio
                        </a>
                    </span>
                    <div class="pull-right">
                        Lo <b>Digital</b> Nos <b>Conecta</b> Usando <a href="https://giosyst3m.com"><b>Web App Integrate</b></a>
                        <small style="font-size:  x-small"><?php echo Yii::app()->params['PowerBy']; ?> - <span class="badge badge-info"><?php echo ENV; ?></span> - <span class="badge badge-info">V <?php echo Yii::app()->params['APP_VERSION']; ?></span></small>
                        <small style="font-size:  x-small">Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a></small>
                    </div>
                    <div class="clearfix"></div>
                </footer>
                <!-- /footer content -->
            </div>
        </div>

        <!-- FastClick -->
        <script src="<?php echo $themePath; ?>/vendors/fastclick/lib/fastclick.js"></script>
        <!-- NProgress -->
        <script src="<?php echo $themePath; ?>/vendors/nprogress/nprogress.js"></script>
        <!-- Custom Theme Scripts -->
        <script src="<?php echo $themePath; ?>/build/js/custom.min.js"></script>
        <!-- ECharts -->
        <script src="<?php echo $themePath; ?>/vendors/echarts/dist/echarts.min.js"></script>
        <script src="<?php echo $themePath; ?>/vendors/echarts/map/js/world.js"></script>
        <!-- jQuery Smart Wizard -->
        <script src="<?php echo $themePath; ?>/vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js"></script>
        <!-- Bootstrap Colorpicker -->
        <script src="<?php echo $themePath; ?>/vendors/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
        <!-- jquery-cookie -->
        <script src="<?php echo $themePath; ?>/vendors/jquery-cookie/src/jquery.cookie.js"></script>
    </body>
</html>
