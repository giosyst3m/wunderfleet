<!DOCTYPE html>
<html lang="en">
    <head>
        <?php if (ENV == 'liv') { ?>
            <!-- Google Tag Manager -->
            <script>(function (w, d, s, l, i) {
                    w[l] = w[l] || [];
                    w[l].push({'gtm.start':
                                new Date().getTime(), event: 'gtm.js'});
                    var f = d.getElementsByTagName(s)[0],
                            j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
                    j.async = true;
                    j.src =
                            'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                    f.parentNode.insertBefore(j, f);
                })(window, document, 'script', 'dataLayer', 'GTM-WX87Z9');</script>
            <!-- End Google Tag Manager -->
        <?php } else { ?>
            <!-- Google Tag Manager -->
            <script>(function (w, d, s, l, i) {
                    w[l] = w[l] || [];
                    w[l].push({'gtm.start':
                                new Date().getTime(), event: 'gtm.js'});
                    var f = d.getElementsByTagName(s)[0],
                            j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
                    j.async = true;
                    j.src =
                            'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                    f.parentNode.insertBefore(j, f);
                })(window, document, 'script', 'dataLayer', 'GTM-5TXSL5');</script>
            <!-- End Google Tag Manager -->
        <?php } ?>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
        <?php
        $cs = Yii::app()->clientScript;
        $themePath = Yii::app()->theme->baseUrl;

        /**
         * StyleSHeets
         */
        $cs
                ->registerCssFile($themePath . '/css/jquery-ui-1.8.19.custom.css')
                ->registerCssFile($themePath . '/css/dashboard/animate.css')
                ->registerCssFile($themePath . '/css/bootstrap-select.css')
                ->registerCssFile($themePath . '/css/bootstrap-timepicker.css')
                ->registerCssFile($themePath . '/css/bootstrap-datetimepicker.css')
                ->registerCssFile($themePath . '/css/ui.jqgrid.css')
                ->registerCssFile($themePath . '/js/jquery.fancybox/jquery.fancybox.css?v=2.1.5')
                ->registerCssFile($themePath . '/js/jquery.fancybox/helpers/jquery.fancybox-buttons.css?v=1.0.5')
                ->registerCssFile($themePath . '/js/jquery.fancybox/helpers/jquery.fancybox-thumbs.css?v=1.0.7')
                ->registerCssFile($themePath . '/css/colors.css')
                ->registerCssFile($themePath . '/css/program.css')
                ->registerCssFile($themePath . '/css/core.css');
        /**
         * JavaScripts
         */
        $cs
                ->registerCoreScript('jquery', CClientScript::POS_END)
                ->registerCoreScript('jquery.ui', CClientScript::POS_END)
                ->registerScriptFile($themePath . '/js/bootstrap.min.js', CClientScript::POS_END)
                ->registerScriptFile($themePath . '/js/dashboard/bootstrap-growl.js', CClientScript::POS_END)
                ->registerScriptFile($themePath . '/js/moment.js', CClientScript::POS_END)
                ->registerScriptFile($themePath . '/js/bootstrap-select.js', CClientScript::POS_END)
                ->registerScriptFile($themePath . '/js/bootstrap-timepicker.js', CClientScript::POS_END)
                ->registerScriptFile($themePath . '/js/bootstrap-datetimepicker.min.js', CClientScript::POS_END)
                ->registerScriptFile($themePath . '/js/grid.locale-es.js', CClientScript::POS_END)
                ->registerScriptFile($themePath . '/js/jquery.jqGrid.min.js', CClientScript::POS_END)
                ->registerScriptFile($themePath . '/vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js', CClientScript::POS_END)
                ->registerScriptFile($themePath . '/vendors/jquery.hotkeys/jquery.hotkeys.js', CClientScript::POS_END)
                ->registerScriptFile($themePath . '/vendors/google-code-prettify/src/prettify.js', CClientScript::POS_END)
                ->registerScriptFile($themePath . '/js/jquery.mousewheel-3.0.6.pack.js', CClientScript::POS_END)
                ->registerScriptFile($themePath . '/js/jquery.fancybox/jquery.fancybox.js?v=2.1.5', CClientScript::POS_END)
                ->registerScriptFile($themePath . '/js/jquery.fancybox/helpers/jquery.fancybox-buttons.js?v=1.0.5', CClientScript::POS_END)
                ->registerScriptFile($themePath . '/js/jquery.fancybox/helpers/jquery.fancybox-thumbs.js?v=1.0.7', CClientScript::POS_END)
                ->registerScriptFile($themePath . '/js/jquery.fancybox/helpers/jquery.fancybox-media.js?v=1.0.6', CClientScript::POS_END)
                ->registerScriptFile($themePath . '/script/core.js', CClientScript::POS_END)
                ->registerScript('tooltip', "$('[data-toggle=\"tooltip\"]').tooltip();
            $('[data-toggle=\"popover\"]').tooltip()"
                        , CClientScript::POS_READY);
        ?>
        <!-- Bootstrap -->
        <link href="<?php echo $themePath; ?>/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="<?php echo $themePath; ?>/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- NProgress -->
        <link href="<?php echo $themePath; ?>/vendors/nprogress/nprogress.css" rel="stylesheet">

        <!-- Custom Theme Style -->
        <link href="<?php echo $themePath; ?>/build/css/custom.min.css" rel="stylesheet">
        <link href='https://fonts.googleapis.com/css?family=Questrial' rel='stylesheet' type='text/css'>
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    </head>

    <body class="login">
        <?php if (ENV == 'liv') { ?>
            <!-- Google Tag Manager (noscript) -->
            <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WX87Z9"
                              height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
            <!-- End Google Tag Manager (noscript) -->
        <?php } else { ?>
            <!-- Google Tag Manager (noscript) -->
            <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5TXSL5"
                              height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
            <!-- End Google Tag Manager (noscript) -->
        <?php } ?>

        <div>
            <a class="hiddenanchor" id="signup"></a>
            <a class="hiddenanchor" id="signin"></a>

            <div class="login_wrapper">
                <div class="animate form login_form">
                    <section class="login_content">
                        <form id="login-form" class="form-horizontal" action="/index.php/site/login" method="post">    
                            <h1>Login</h1>
                            <div class="form-group has-error">
                                <label class="control-label required" for="LoginForm_username">Username <span class="required">*</span></label>
                                <div>
                                    <input name="LoginForm[username]" id="LoginForm_username" class="form-control" placeholder="" type="text">
                                    <p id="LoginForm_username_em_" style="" class="help-block">Username cannot be blank.</p>
                                </div>
                            </div>
                            <div class="form-group has-success">
                                <label class="control-label required" for="LoginForm_password">Password <span class="required">*</span></label>
                                <div>
                                    <input name="LoginForm[password]" id="LoginForm_password" class="form-control" placeholder="" type="password">
                                    <p id="LoginForm_password_em_" style="display:none" class="help-block"></p>
                                </div>
                            </div>
                            <div class="checkbox"><input id="ytLoginForm_rememberMe" type="hidden" value="0" name="LoginForm[rememberMe]">
                                <label class="control-label">
                                    <input name="LoginForm[rememberMe]" id="LoginForm_rememberMe" value="1" type="checkbox">Recordad mi sessión para la siguiente vez<p id="LoginForm_rememberMe_em_" style="display:none" class="help-block"></p></label></div>
                            <button class="btn btn-primary" type="submit" name="yt0"><span class="glyphicon glyphicon-user"></span> Entrar al sistema</button>    
                            <a id="id-IN-" log="/index.php/entidad/HabLog/checkIN/id/1/status/2/log/1" class="btn btn-danger" href="#">
                                <span class="glyphicon glyphicon-exclamation-sign">

                                </span> Olvide Contraseña</a>

                            <div class="clearfix"></div>

                            <div class="separator">
                                <p class="change_link">New to site?
                                    <a href="#signup" class="to_register"> Create Account </a>
                                </p>

                                <div class="clearfix"></div>
                                <br />

                                <div>
                                    <span style="font-size: xx-large"> <a href="<?php echo Yii::app()->createUrl('site'); ?>"><i class="fa fa-home"></i> <span>Digital Conecta</span></a></span>
                                    <p>Lo <b>Digital</b> Nos <b>Conecta</b> Usando <a href="https://digitalconecta.com"><b>Sistemas Integrados</b></a>
                                        <small style="font-size:  x-small"><?php echo Yii::app()->params['PowerBy']; ?> - <span class="badge badge-info"><?php echo ENV; ?></span> - <span class="badge badge-info">V <?php echo Yii::app()->params['APP_VERSION']; ?></span></small>
                                    </p>
                                </div>
                            </div>
                        </form>
                    </section>
                </div>

                <div id="register" class="animate form registration_form">
                    <section class="login_content">
                        <form>
                            <h1>Create Account</h1>
                            <div>
                                <input type="text" class="form-control" placeholder="Username" required="" />
                            </div>
                            <div>
                                <input type="email" class="form-control" placeholder="Email" required="" />
                            </div>
                            <div>
                                <input type="password" class="form-control" placeholder="Password" required="" />
                            </div>
                            <div>
                                <a class="btn btn-default submit" href="index.html">Submit</a>
                            </div>

                            <div class="clearfix"></div>

                            <div class="separator">
                                <p class="change_link">Already a member ?
                                    <a href="#signin" class="to_register"> Log in </a>
                                </p>

                                <div class="clearfix"></div>
                                <br />

                                <div>
                                    <h1><i class="fa fa-paw"></i> Gentelella Alela!</h1>
                                    <p>©2016 All Rights Reserved. Gentelella Alela! is a Bootstrap 3 template. Privacy and Terms</p>
                                </div>
                            </div>
                        </form>
                    </section>
                </div>
            </div>
        </div>
        <?php
        $this->widget('bootstrap.widgets.BsModal', array(
            'id' => 'myModal',
            'header' => Yii::t('app', 'Recuperar Contraseña: Escriba el email que tiene registrado en el sistema'),
            'footer' => array(
                BsHtml::ajaxButton(
                        Yii::t('app', 'OK'), Yii::app()->createUrl('sistema/usuario/recoveryPassword/'), array(
                    'type' => 'post',
                    'dataType' => 'html',
                    'data' => 'js:{"email":$("#emailX").val()}',
                    'success' => 'function(data){ $("#div-resp").html(data) }',
                        ), array(
                    'class' => 'btn btn-success',
                    'color' => BsHtml::BUTTON_COLOR_SUCCESS,
                    'icon' => BsHtml::GLYPHICON_SEND
                        )
                ),
                BsHtml::button(
                        Yii::t('app', 'Cancel'), array(
                    'data-dismiss' => 'modal',
                    'icon' => BsHtml::GLYPHICON_REMOVE
                        )
                )
            )
                )
        );
        ?>
        <script type="text/javascript">
            /*<![CDATA[*/

            jQuery(function ($) {
                jQuery('body').on('click', '#id-IN-', function () {
                    jQuery.ajax({'cache': true, 'data': {'message': 'Proccess...'}, 'type': 'POST', 'success': function (data) {
                            $(".modal-body").html(data);
                            $("#myModal").modal("show");
                        }, 'url': '/index.php/sistema/usuario/getForm'});
                    return false;
                });
                jQuery('#login-form').yiiactiveform({'validateOnSubmit': true, 'attributes': [{'id': 'LoginForm_username', 'inputID': 'LoginForm_username', 'errorID': 'LoginForm_username_em_', 'model': 'LoginForm', 'name': 'username', 'enableAjaxValidation': false, 'inputContainer': 'div.form-group', 'errorCssClass': 'has-error', 'successCssClass': 'has-success', 'clientValidation': function (value, messages, attribute) {

                                if (jQuery.trim(value) == '') {
                                    messages.push("Username cannot be blank.");
                                }

                            }}, {'id': 'LoginForm_password', 'inputID': 'LoginForm_password', 'errorID': 'LoginForm_password_em_', 'model': 'LoginForm', 'name': 'password', 'enableAjaxValidation': false, 'inputContainer': 'div.form-group', 'errorCssClass': 'has-error', 'successCssClass': 'has-success', 'clientValidation': function (value, messages, attribute) {

                                if (jQuery.trim(value) == '') {
                                    messages.push("Password cannot be blank.");
                                }

                            }}, {'id': 'LoginForm_rememberMe', 'inputID': 'LoginForm_rememberMe', 'errorID': 'LoginForm_rememberMe_em_', 'model': 'LoginForm', 'name': 'rememberMe', 'enableAjaxValidation': false, 'inputContainer': 'div.form-group', 'errorCssClass': 'has-error', 'successCssClass': 'has-success', 'clientValidation': function (value, messages, attribute) {

                                if (jQuery.trim(value) != '' && value != "1" && value != "0") {
                                    messages.push("Recordad mi sessi\u00f3n para la siguiente vez must be either 1 or 0.");
                                }

                            }}], 'errorCss': 'error'});
                jQuery('body').on('click', '#yt1', function () {
                    jQuery.ajax({'type': 'post', 'dataType': 'html', 'data': {"email": $("#emailX").val()}, 'success': function (data) {
                            $("#div-resp").html(data)
                        }, 'url': '/index.php/sistema/usuario/recoveryPassword', 'cache': false});
                    return false;
                });
                $('[data-toggle="tooltip"]').tooltip();
                $('[data-toggle="popover"]').tooltip()
            });
            /*]]>*/
        </script>
    </body>
</html>
