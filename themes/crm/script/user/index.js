
$(document).ready(function () {
    $.cookie.json = true;
    $.cookie.raw = true;

    var ck = $.cookie();

    if (!jQuery.isEmptyObject(ck.w_id)) {
        getData(ck.w_id.split('"')[1]);
    }
    // Smart Wizard         
    $('#wizard').smartWizard({
        onLeaveStep: leaveAStepCallback,
        onFinish: onFinishCallback
    });

    function leaveAStepCallback(obj, context) {
        //alert("Leaving step " + context.fromStep + " to go to step " + context.toStep);
        return validateSteps(context.fromStep); // return false to stay on step and true to continue navigation 
    }

    function onFinishCallback(objs, context) {
        if (validateAllSteps()) {
            //$('form').submit();
        }
    }

    // Your Step validation logic
    function validateSteps(stepnumber) {
        var isStepValid = false;

        // validate step 1
        if (stepnumber == 1) {
            url = 'user/validPersonal/id/' + $('#_id').val();
            form = 'form#user-form';
            message = '#user-form_es_';
        } else if (stepnumber == 2) {
            url = 'user/validAddress/id/' + $('#_id').val();
            form = 'form#user-form-address';
            message = '#user-form-address_es_';
        } else if (stepnumber == 3) {
            url = 'user/validAccount/id/' + $('#_id').val();
            form = 'form#user-form-account';
            message = '#user-form-account_es_';
        }

        $.ajax({
            url: getHomeUrl() + url,
            type: "post",
            dataType: "json",
            async: false,
            data: $(form).serialize(),
            success: function (data, textStatus, jqXHR)
            {
                $.growl(data.menssage, {
                    type: data.type
                });
                if (data.code == 400) {
                    $(message + ' ul').remove();
                    $.each(data.data, function (index, value) {
                        $('#User_' + index + '_em_').append(value).show();
                        $(message).append('<ul><li>' + value + '</li></ul>').show();
                    });
                    isStepValid = false;
                } else {
                    $(message).hide();
                    $('#_id').val(data.data._id);
                    $.cookie('w_id', data.data._id, {expires: 7, path: '/'});
                    if (stepnumber == 3) {
                        if (data.data.payment.status >= 200 && data.data.payment.status < 300) {
                            $('#success').show();
                            $('#danger').hide();
                            $('#success').html(data.data.payment.paymentDataId);
                            $('#error_user').text('Payment Information');
                            $('#succes_buttom').show();
                        } else {
                            $('#success').hide();
                            $('#succes_buttom').hide();
                            $('#danger').show();
                            $.growl(data.data.payment.status + ' - ' + data.data.payment.status_desc + ' - ' + data.data.payment.message, {
                                type: 'error',
                                message: data.data.payment.message
                            });
                            $('#error_user').text('Error: ' + data.data.payment.status + ' - ' + data.data.payment.message);
                            $('#account_user').text($('#User_account').val());
                            $('#iban_user').text($('#User_iban').val());
                        }
                    }
                    isStepValid = true;
                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                $.growl("App Error " + errorThrown, {
                    type: "danger"
                });
            }
        });
        return isStepValid;
        // ...      
    }
    function validateAllSteps() {
        var isStepValid = true;
        // all step validation logic     
        return isStepValid;

    }

    function getData(id) {
        $('#_id').val(id);
        $.ajax({
            url: getHomeUrl() + 'user/getData/id/' + id,
            type: "post",
            dataType: "json",
            success: function (data, textStatus, jqXHR)
            {
                $.growl(data.menssage, {
                    type: data.type
                });
                $('._id').val(id);
                $('#User_name').val(data.data.name);
                $('#User_lastname').val(data.data.lastname);
                $('#User_phone').val(data.data.phone);
                $('#User_address').val(data.data.address);
                $('#User_additional').val(data.data.additional);
                $('#User_zip').val(data.data.zip);
                $('#User_city').val(data.data.city);
                $('#User_account').val(data.data.account);
                $('#User_iban').val(data.data.iban);


                $("#wizard").smartWizard('goToStep', data.data.step);
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                $.growl("App Error " + errorThrown, {
                    type: "danger"
                });
            }
        });
    }
});

function lastStep(bln) {
    if (bln) {
        $.removeCookie('w_id', {expires: 7, path: '/'} );
        window.location.href = getHomeUrl() + 'site';
    } else {
        $("#wizard").smartWizard('goToStep', 3);
    }
}