<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $id
 * @property string $_id
 * @property string $name
 * @property string $lastname
 * @property string $phone
 * @property integer $step
 * @property string $address
 * @property string $additional
 * @property string $zip
 * @property string $city
 * @property string $account
 * @property string $iban
 * @property string $payment
 * @property string $r_c_d
 * @property integer $r_d_s
 */
class User extends GS3CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, lastname', 'required', 'on' => 'personal'),
			array('address, additional, zip, city', 'required', 'on' => 'address'),
                        array('account, iban', 'required', 'on' => 'account'),
			array('step, r_d_s', 'numerical', 'integerOnly'=>true),
			array('_id, name, lastname, city, account', 'length', 'max'=>50),
			array('phone, additional, iban', 'length', 'max'=>15),
			array('address, zip', 'length', 'max'=>25),
			array('payment', 'length', 'max'=>500),
			array('r_c_d', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, _id, name, lastname, phone, step, address, additional, zip, city, account, iban, payment, r_c_d, r_d_s', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('app','ID'),
			'_id' => Yii::t('app','ID'),
			'name' => Yii::t('app','First Name'),
			'lastname' => Yii::t('app','Last name'),
			'phone' => Yii::t('app','Telephone'),
			'step' => Yii::t('app','Step'),
			'address' => Yii::t('app','Address'),
			'additional' => Yii::t('app','house/flat Number'),
			'zip' => Yii::t('app','Zip Code'),
			'city' => Yii::t('app','City'),
			'account' => Yii::t('app','Account Owner'),
			'iban' => Yii::t('app','IBAN'),
			'payment' => Yii::t('app','Payment'),
			'r_c_d' => Yii::t('app','R C D'),
			'r_d_s' => Yii::t('app','R D S'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('_id',$this->_id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('lastname',$this->lastname,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('step',$this->step);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('additional',$this->additional,true);
		$criteria->compare('zip',$this->zip,true);
		$criteria->compare('city',$this->city,true);
		$criteria->compare('account',$this->account,true);
		$criteria->compare('iban',$this->iban,true);
		$criteria->compare('payment',$this->payment,true);
		$criteria->compare('r_c_d',$this->r_c_d,true);
		$criteria->compare('r_d_s',$this->r_d_s);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
