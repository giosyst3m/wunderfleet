<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php echo "<?php\n"; ?>
/* @var $this <?php echo $this->getControllerClass(); ?> */
/* @var $data <?php echo $this->getModelClass(); ?> */
?>
<div id="users-cont" class="box row1 g6">
    <div class="scroll">
        <ul class="scroll-cont ul-grad ui-sortable">

            <?php
            echo "\t<li><span><?php echo CHtml::encode(\$data->getAttributeLabel('{$this->tableSchema->primaryKey}')); ?>:</span>\n";
            echo "\t<span class='users-role'><?php echo CHtml::link(CHtml::encode(\$data->{$this->tableSchema->primaryKey}), array('view', 'id'=>\$data->{$this->tableSchema->primaryKey})); ?>\n\t</span></li>\n\n";
            $count=0;

            foreach($this->tableSchema->columns as $column)
            {
                if($column->isPrimaryKey)
                    continue;
                if(++$count==7)
                    echo "\t<?php /*\n";
                echo "\t<li><span><?php echo CHtml::encode(\$data->getAttributeLabel('{$column->name}')); ?>:</span>\n";
                echo "\t<span class='users-role'><?php echo CHtml::encode(\$data->{$column->name}); ?>\n\t</span></li>\n\n";
            }
            if($count>=7)
                echo "\t*/ ?>\n";
            ?>
        </ul>
    </div>
</div>