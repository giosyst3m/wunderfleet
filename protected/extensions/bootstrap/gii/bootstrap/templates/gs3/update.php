<?php
/**
 * The following variables are available in this template:
 * - $this: the BootstrapCode object
 */
?>
<?php echo "<?php\n"; ?>
/* @var $this <?php echo $this->getControllerClass(); ?> */
/* @var $model <?php echo $this->getModelClass(); ?> */
<?php echo "?>\n"; ?>

<?php
echo "<?php\n";
$nameColumn = $this->guessNameColumn($this->tableSchema->columns);
$label = $this->pluralize($this->class2name($this->modelClass));
echo "\$this->breadcrumbs=array(
	'$label'=>array('create'),
	\$model->{$nameColumn}=>array('view','id'=>\$model->{$this->tableSchema->primaryKey}),
	'Update',
);\n";
?>

?>

<?php echo "<?php echo BsHtml::pageHeader(Yii::t('app','Update'),Yii::t('app','$this->modelClass').' Nro: '.\$model->{$this->tableSchema->primaryKey}) ?>\n"; ?>

<?php echo "<?php 
if(Yii::app()->user->checkAccess('".$this->modelClass."UpdateFormView')){
    \$this->renderPartial('_form', array('model'=>\$model)); 
}
?>\n"?>
<hr>
<?php echo "<?php 
if(Yii::app()->user->checkAccess('".$this->modelClass."UpdateAdminView')){
    \$this->renderPartial('admin', array('model2'=>\$model2)); 
}
?>"?>