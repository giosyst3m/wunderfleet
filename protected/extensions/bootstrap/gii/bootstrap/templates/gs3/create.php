<?php
/**
 * The following variables are available in this template:
 * - $this: the BootstrapCode object
 */
?>
<?php echo "<?php\n"; ?>
/* @var $this <?php echo $this->getControllerClass(); ?> */
/* @var $model <?php echo $this->getModelClass(); ?> */
<?php echo "?>\n"; ?>

<?php
echo "<?php\n";
$label = $this->pluralize($this->class2name($this->modelClass));
echo "\$this->breadcrumbs=array(
	'$label'=>array('create'),
	'Create',
);\n";
?>

?>

<?php echo "<?php echo BsHtml::pageHeader(Yii::t('app','Create'),Yii::t('app','$this->modelClass')) ?>\n"; ?>

<?php echo "<?php 
if(Yii::app()->user->checkAccess('".$this->modelClass."CreateFormView')){
    \$this->renderPartial('_form', array('model'=>\$model)); 
}
?>\n"?>
<hr>
<?php echo "<?php 
if(Yii::app()->user->checkAccess('".$this->modelClass."CreateAdminView')){
    \$this->renderPartial('admin', array('model2'=>\$model2)); 
}
?>"?>
