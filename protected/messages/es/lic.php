<?php

return [
    'LICENCE_USER'          =>  'Nro. de Usuarios',
    'LICENCE_CLIENT'        =>  'Nro. de clientes',
    'LICENCE_CLIENT_ACCESS' =>  'Clientes creados para acceder al sistema',
    'LICENCE_SELLER'        =>  'Nro. de Vendedores',
    'LICENCE_ORDER'         =>  'Nro. de Pedidos creados',
    'LICENCE_SUPPORT'       =>  'Soporte técnico',
    'LICENCE_MODULES'       =>  'Módulos personalizados, procesos, funcionalidad',
    'LICENCE_MULTICOMPANY'  =>  'Multiempresa',
    'LICENCE_CUSTUM_REPORTS'=>  'Creación de reportes personalizados',
    'LICENCE_VERSION'       =>  'Versión',
    'LICENCE_TYPE'          =>  'Plan',
    'PROGRAM_TITLE'         =>  'GioSyst3m Inventario',
    'PROGRAM_DESCRIPTION'   =>  '',
    'LICENCE_CNAME'         =>  'Crear un alias a su dominio.'
];