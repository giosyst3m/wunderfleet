<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

return array(
    'Task'=>'Tarea',
    'Role'=>'Rol',
    'Operation'=>'Operación',
    'name'=>'Nombre',
    'description'=>'Descripión',
    'bizrule'=>'Regla de Negocio',
    'data'=>'Datos',
    'User'=>'Usuario',
    'Assignment'=>'Asignación',
    'SAVE'=>'El {type}: {dato} ha sido guardado sastifactoriamente',
    'DUPLICATE'=>'El rol </b>{type}: </b>{dato} ya esta existe ',
    'DENEY'=>'Ud no posee permisos para <b>{accion}</b>'
);