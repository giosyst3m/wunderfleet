<?php

return array(
    'Email-2000'    =>      'Notificación enviada por email',
    'Email-2001'    =>      'Orden Nro. {numero}: {status}',
    'Email-2002'    =>      'Orden Nro. {numero}: {status} fue notificado a {grupo}',
    'Email-2003'    =>      'Orden Nro. {numero}: Se ha recibido un comentario Titulo: {title}, Prioridad: {priority} ',
    'Email-2004'    =>      'Notifiación de Creación de cliente Notificado Correcta {grupo}',
    'Email-2005'    =>      'Ticket Generado en el Sistema {numero} Prioridad {priority} del SubDominio {sub_dominio}',
    
    /*500*/
    'Email-5001'    =>      'Error al enviar notifiación de la creación de la orden Nro. {numero}',
    'Email-5002'    =>      'Error al enviar notifiación Comentario de la orden Nro. {numero}, Titulo: {title}',
    'Email-5003'    =>      'Error al tratar de enviar la notificación de creación del cliente',
    'Email-5004'    =>      'Error al enviar notifiación de la creación de la Ticket Nro. {numero}',
);

