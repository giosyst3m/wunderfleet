<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
error_reporting(E_ALL);
ini_set('display_errors', '1');

return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'Web App Integrate - GioSyst3m',
    'theme' => 'crm',
    // preloading 'log' component
    'preload' => array('log', 'yiibooster', 'chartjs'),
    'language' => 'es',
    'sourceLanguage' => 'en',
    'timeZone' => 'America/Bogota',
    // autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.components.*',
        'bootstrap.behaviors.*',
        'bootstrap.helpers.*',
        'bootstrap.widgets.*',
        'ext.YiiMailer.YiiMailer',
        'ext.typeahead.TbTypeAhead',
        'ext.yii-chartjs.*',
        'ext.LiveGridView.*',
    ),
    // path aliases
    'aliases' => array(
        'bootstrap' => 'ext.bootstrap',
        'chartjs' => 'ext.yii-chartjs'
    ),
    'modules' => array(
        // uncomment the following to enable the Gii tool

        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => 'cucuta00',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters' => array('127.0.0.1', '::1'),
            'generatorPaths' => array(
                'ext.gii', 'bootstrap.gii'
            )
        ),
    ),
    // application components
    'components' => array(
        'mobileDetect' => array(
            'class' => 'ext.MobileDetect.MobileDetect'
        ),
        /**'cache' => array(
            'class' => ENV == 'local' ? 'CFileCache' : 'CMemCache',
        ),**/
        'yexcel' => array(
            'class' => 'ext.yexcel.Yexcel'
        ),
        'user' => array(
            // enable cookie-based authentication
            'allowAutoLogin' => true,
            'loginUrl' => array('site/login'),
        ),
        'authManager' => array(
            'class' => 'CDbAuthManager',
            'connectionID' => 'db',
            'itemTable' => 'auth_item', // Tabla que contiene los elementos de autorizacion
            'itemChildTable' => 'auth_item_child', // Tabla que contiene los elementos padre-hijo
            'assignmentTable' => 'auth_assignment', // Tabla que contiene la signacion usuario-autorizacion
        ),
        'bootstrap' => array(
            'class' => 'bootstrap.components.BsApi',
        ),
        'chartjs' => array('class' => 'chartjs.components.ChartJs'),
        // uncomment the following to enable URLs in path-format
        'urlManager' => array(
            'urlFormat' => 'path',
//            'showScriptName' => false,
//            'caseSensitive' => false,
            'rules' => array(
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ),
        ),
        'db' => require_once dirname(__FILE__) . '/db-' . ENV . '.php', // Database
       'coreMessages' => array(
            'basePath' => 'protected/messages'
        ),
        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning, trace, info, profile',
                ),
                // uncomment the following to show log messages on web pages
//				array(
//					'class'=>'CWebLogRoute',
//                    'enabled'=>YII_DEBUG,
//                    'levels'=>'error, warning, trace, info, profile',
////                    'filter'=>'CLogFilter',
//				),
                array(
                    'class' => 'CDbLogRoute',
//                    'enabled'=>!YII_DEBUG,
                    'levels' => 'error, warning, info, SisUsuario',
                    'connectionID' => 'db',
                    'logTableName' => 'sis_log',
                ),
                array(
                    'class' => 'CEmailLogRoute',
//                    'enabled'=>!YII_DEBUG,
                    'levels' => 'error, warning, SisUsuario',
                    'emails' => array(
                        'sistemas@giosyst3m.net',
                    ),
                    'subject' => 'Warning / error / info / SisUsuario',
                    'sentFrom' => 'sistemas@giosyst3m.net',
                ),
//				array(
//					'class'=>'CProfileLogRoute',
//                    'enabled'=>YII_DEBUG,
//                    'levels'=>'error, warning, trace, info, profile',
//
//				),
            ),
        ),
        'zip' => array(
            'class' => 'application.extensions.zip.EZip',
        ),
        //...
        'ePdf' => array(
            'class' => 'ext.yii-pdf.EYiiPdf',
            'params' => array(
                'mpdf' => array(
                    'librarySourcePath' => 'application.vendor.MPDF57.*',
                    'constants' => array(
                        '_MPDF_TEMP_PATH' => Yii::getPathOfAlias('application.runtime'),
                    ),
                    'class' => 'mpdf', // the literal class filename to be loaded from the vendors folder
                    'defaultParams' => array(// More info: http://mpdf1.com/manual/index.php?tid=184
                        'mode' => '', //  This parameter specifies the mode of the new document.
                        'format' => 'Letter', // format A4, A5, ...
                        'default_font_size' => 0, // Sets the default document font size in points (pt)
                        'default_font' => '', // Sets the default font-family for the new document.
                        'mgl' => 15, // margin_left. Sets the page margins for the new document.
                        'mgr' => 15, // margin_right
                        'mgt' => 16, // margin_top
                        'mgb' => 16, // margin_bottom
                        'mgh' => 9, // margin_header
                        'mgf' => 9, // margin_footer
                        'orientation' => 'l', // landscape or portrait orientation
                    )
                ),
                'HTML2PDF' => array(
                    'librarySourcePath' => 'application.vendor.html2pdf_v403.*',
                    'classFile' => 'html2pdf.class.php', // For adding to Yii::$classMap
                /* 'defaultParams'     => array( // More info: http://wiki.spipu.net/doku.php?id=html2pdf:en:v4:accueil
                  'orientation' => 'P', // landscape or portrait orientation
                  'format'      => 'A4', // format A4, A5, ...
                  'language'    => 'en', // language: fr, en, it ...
                  'unicode'     => true, // TRUE means clustering the input text IS unicode (default = true)
                  'encoding'    => 'UTF-8', // charset encoding; Default is UTF-8
                  'marges'      => array(5, 5, 5, 8), // margins by default, in order (left, top, right, bottom)
                  ) */
                )
            ),
        ),
    ),
    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params' => array(
        // this is used in contact page
        'adminEmail' => 'info@digitalconecta.com',
        'emailSupport' => 'info@digitalconecta.com',
        'emailSupportPedidoBcc' => true,
        'emailSupportUserBcc' => true,
        'urlSupport' => 'http://www.giosyst3m.net',
        'NumeroHabitacionesPredeteminada' => 20,
        'URL_PRODUCTO_IMAGEN' => '/files/product/img/',
        'URL_PRODUCTO_FILE' => '/files/product/file/',
        'URL_USUARIO_IMAGEN' => '/files/usuario/fotos/',
        'URL_CLIENT_LOGO' => '/files/client/logo/',
        'URL_CLIENTE_FILE' => '/files/client/file/',
        'CACHE_TIME' => 10,
        'CRYPT_SALT' => '@$',
        'LPAD' => 5,
//        'emailDespachoUserID' => '1',
        'PowerBy' => 'Power by <span class="fa fa-twitter"></span> @Giosyst3m <span class="fa fa-link"></span> <a href="http://www.giosyst3m.net" target="_blank">http://www.giosyst3m.net</a> <i class="fa fa-envelope-o"></i> <a href="mailto:info@giosyst3m.com">info@giosyst3m.net</a>',
        'PowerBy_PDF' => '@Giosyst3m http://www.giosyst3m.net info@giosyst3m.net',
//        'INVENTARIO_DELETE_PRODUCTO' => FALSE,
//        'TARIFAS_HABITACION_PRINCIPALES' => array(1, 4),
//        'REGISTRO_CLIENTE_CHECK_IN' => false,
        'DASHBOARD_ACTUALIZACION_TIEMPOS_VALES' => 180000, //3 min,
//        'DASHBOARD_ACTUALIZACION_TIEMPOS_PEDIDOS' => 180000, //3 min
//        'DASHBOARD_ACTUALIZACION_TIEMPOS_CAJA' => 180000, //3 min
//        'SISTEMA_MANTENIMIENTO'=>false,
        'JSON_API_VERSION' => 1,
        'APP_VERSION' => '0.0.1'
    ),
);


