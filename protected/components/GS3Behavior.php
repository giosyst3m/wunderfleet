<?php

class GS3Behavior extends CActiveRecordBehavior {
	
    protected $r_c_u = 'r_c_u';
    protected $r_c_d = 'r_c_d';
    protected $r_c_i = 'r_c_i';
    protected $r_u_u = 'r_u_u';
    protected $r_u_d = 'r_u_d';
    protected $r_u_i = 'r_u_i';
    protected $r_d_u = 'r_d_u';
    protected $r_d_d = 'r_d_d';
    protected $r_d_i = 'r_d_i';

    public function beforeSave($event) {
        Yii::app()->db->createCommand("SET time_zone = '-5:00'")->execute();
        if ($this->getOwner()->getIsNewRecord()) {
            if($this->getOwner()->hasAttribute($this->r_c_u)){
                $this->getOwner()->{$this->r_c_u} = Yii::app()->user->id;
            }
            if($this->getOwner()->hasAttribute($this->r_c_i)){
                $this->getOwner()->{$this->r_c_i} = Yii::app()->request->userHostAddress;
            }
            if($this->getOwner()->hasAttribute($this->r_c_d)){
                $this->getOwner()->{$this->r_c_d} = date('Y-m-d H:i:s');
            }
        }else{
            if($this->getOwner()->hasAttribute($this->r_u_d)){
                $this->getOwner()->{$this->r_u_d} = date('Y-m-d H:i:s');
            }
            if($this->getOwner()->hasAttribute($this->r_u_u)){
                $this->getOwner()->{$this->r_u_u} = Yii::app()->user->id;
            }
            if($this->getOwner()->hasAttribute($this->r_u_i)){
                $this->getOwner()->{$this->r_u_i} = Yii::app()->request->userHostAddress;
            }
        }
	}

}