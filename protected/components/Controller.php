<?php

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController {

    /**
     * @var string the default layout for the controller view. Defaults to '//layouts/column1',
     * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
     */
    public $layout = '//layouts/column1';

    /**
     * @var array context menu items. This property will be assigned to {@link CMenu::items}.
     */
    public $menu = array();

    /**
     * @var array the breadcrumbs of the current page. The value of this property will
     * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
     * for more details on how to specify this property.
     */
    public $breadcrumbs = array();

    protected function beforeAction($action) {
        spl_autoload_unregister(array('YiiBase', 'autoload'));
        Yii::import('ext.yexcel.Classes.PHPExcel', true);
        spl_autoload_register(array('YiiBase', 'autoload'));
        return parent::beforeAction($action);
    }

    /**
     * Get JSon structure 
     * @param string $status_code Sucess, Error, Fail
     * @param string $status_msg Personal Mensaje
     * @param array $data Data
     * @param array $items records
     * @param int $error_code Internal Code
     * @param string $erro_msg Mesasage descriptio
     * @param array $error_data aditional information
     * @param array $options addition option
     * @param boolen $header send Hedaer Json
     * @return json
     */
    function getJSon($status_code, $status_msg = '', $data = array(), $items = array(), $error_code = 0, $erro_msg = '', $error_data = array(), $options = array(), $header = true) {
        $json = array();
        if ($header) {
            $json = array_merge($json, array(
                'apiVersion' => 1
                    )
            );
        }
        if (!empty($data)) {
            if (is_object($data)) {
                $data = CJSON::decode(CJSON::encode($data));
            }
            $json = array_merge($json, array('data'=> $data));
        }
        switch (strtolower($status_code)) {
            case 100:
                $status = 'info';
                $type = 'info';
                $fontSize = '14px';
                break;
            case 200:
                $status = 'success';
                $type = 'success';
                $fontSize = '14px';
                break;
            case 500:
                $status = 'error';
                $type = 'danger';
                $fontSize = '14px';
                break;
            case 400:
                $status = 'fail';
                $type = 'warning';
                $fontSize = '14px';
                break;
            default:
                $status = 'undefined';
                $type = 'info';
                $fontSize = '14px';
                break;
        }
        $json = array_merge($json, array(
            'status' => $status,
            'code' => $status_code,
            'menssage' => '<span style="font-size:' . $fontSize . '">' . Yii::t('json', $status_msg) . '</span>',
            'type' => $type,
                )
        );
        if (!empty($options)) {
            $json = array_merge($json, array(
                'options' => $options,
                    )
            );
        }
        if (!empty($items)) {
            $json = array_merge($json, array(
                'items' => $items,
                    )
            );
        }
        if (!empty($error_code)) {
            $json = array_merge($json, array(
                'error' => array(
                    'code' => $error_code,
                    'message' => Yii::t('json', $erro_msg),
                    'errors' => $error_data
                ),
                    )
            );
        }

        return CJSON::encode($json);
    }

    /**
     * Get Randor leters and numbers
     * @param int $length
     * @return string
     */
    function generateRandomString($length = 10, $upper = false) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        if ($upper) {
            return strtoupper($randomString);
        } else {
            return $randomString;
        }
    }

    /**
     * Crear el menu segun el id del programa que se recibe.
     * @author Giovanni Ariza <sistemas@giosyst3m.net>
     * @since 2014-06-17
     * @version 1.0.0
     * @param int $programa
     */
    public function MegaMenu($programa) {
        if (!Yii::app()->user->isGuest) {
            $SisMenu = SisMenu::model()->cache(3600, new CDbCacheDependency('SELECT COUNT(*) FROM sis_menu'))->findAll('id_sis_programa=:programa AND padre=0 AND r_d_s=1 ORDER BY orden', array(':programa' => $programa));
        } else {
            $items = array();
        }
        $menu = '';
        echo $menu;
    }

    public function MegaMenuSubOpciones($padre) {
//        $SisMenuSub = SisMenu::model()->findAll('padre=:padre AND r_d_s=1  ORDER BY orden', array(':padre' => $padre));
        $SisMenuSub = SisMenu::model()->cache(3600, new CDbCacheDependency('SELECT COUNT(*) FROM sis_menu'))->findAll('padre=:padre AND r_d_s=1  ORDER BY orden', array(':padre' => $padre));
        $Submenu = BsHtml::openTag('ul', array('class' => "dropdown-menu mega-dropdown-menu row"));

        foreach ($SisMenuSub as $items) {
            $Submenu .= '<li class="col-sm-3">
                            <ul>
                                <li class="dropdown-header">' . $items->nombre . '</li>
                                <li><a href="#">Unique Features</a>
                                </li>
                                <li><a href="#">Image Responsive</a>
                                </li>
                                <li><a href="#">Auto Carousel</a>
                                </li>
                                <li><a href="#">Newsletter Form</a>
                                </li>
                                <li><a href="#">Four columns</a>
                                </li>
                            </ul>
                        </li>';
//            $Submenu .= BsHtml::openTag('li',array('class'=>"col-sm-3")); 
//                $Submenu .= BsHtml::openTag('ul');
//                    $Submenu .= BsHtml::tag('li',array('class'=>"dropdown-header"),$items->nombre,true);
//                    $Submenu .= '
//                        <li><a href="#">Unique Features</a>
//                        </li>
//                        <li><a href="#">Image Responsive</a>
//                        </li>
//                        <li><a href="#">Auto Carousel</a>
//                        </li>
//                        <li><a href="#">Newsletter Form</a>
//                        </li>
//                        <li><a href="#">Four columns</a>
//                        </li>';
//                $Submenu .= BsHtml::closeTag('ul');
//            $Submenu .= BsHtml::closeTag('li');
        }

        $Submenu .= BsHtml::closeTag('ul');
        return $Submenu;
    }

    public function Menu($programa) {
        if (!Yii::app()->user->isGuest) {
            //Obtener las opciones segun el programa sercibido
            $SisMenu = SisMenu::model()->cache(3600, new CDbCacheDependency('SELECT COUNT(*) FROM sis_menu'))->findAll('id_sis_programa=:programa AND padre=0 AND r_d_s=1 ORDER BY orden', array(':programa' => $programa));
            //Ciclo para crear el menu
            for ($index = 0; $index < count($SisMenu); $index++) {
                //Inicializar la etiqueta
                $items[$index]['label'] = $SisMenu[$index]->nombre;
                //Inicializar la URL
                $items[$index]['url'] = array(Yii::app()->homeUrl . $SisMenu[$index]->link);
                //Inicializar el icono
                $items[$index]['icon'] = $SisMenu[$index]->icono;
                //Crear las sub-opciones
                $items[$index]['items'] = $this->MenuSubOpciones($SisMenu[$index]->id);
            }
        } else {
            $items = array();
        }
        //Crear el Menu por el Widget con Boostrap
        $this->widget('bootstrap.widgets.BsNavbar', array(
            'collapse' => true,
            'position' => BsHtml::NAVBAR_POSITION_FIXED_TOP,
            'brandLabel' => BsHtml::icon(BsHtml::GLYPHICON_HOME),
            'brandUrl' => Yii::app()->homeUrl,
            'color' => BsHtml::NAVBAR_COLOR_INVERSE,
            'items' => array(
                array(
                    'class' => 'bootstrap.widgets.BsNav',
                    'type' => 'navbar',
                    'activateParents' => true,
                    'items' => $items
                ),
                array(
                    'class' => 'bootstrap.widgets.BsNav',
                    'type' => 'navbar',
                    'activateParents' => true,
                    'items' => array(
                        array(
                            'icon' => BsHtml::GLYPHICON_ENVELOPE,
                            'title' => '',
                            'url' => array(
                                '/sistema/contacto/create'
                            )
                        ),
                        array(
                            'icon' => BsHtml::GLYPHICON_USER,
                            'title' => '',
                            'url' => array(
                                '/sistema/usuario/index'
                            ),
                            'visible' => !Yii::app()->user->isGuest
                        ),
                        array(
                            'label' => 'Entrar',
                            'icon' => BsHtml::GLYPHICON_OFF,
                            'url' => array(
                                '/site/login'
                            ),
                            'pull' => BsHtml::NAVBAR_NAV_PULL_RIGHT,
                            'visible' => Yii::app()->user->isGuest
                        ),
                        array(
                            'icon' => BsHtml::GLYPHICON_REMOVE_CIRCLE,
                            'label' => 'Salir (' . Yii::app()->user->name . ')',
                            'pull' => BsHtml::NAVBAR_NAV_PULL_RIGHT,
                            'url' => array(
                                '/site/logout'
                            ),
                            'visible' => !Yii::app()->user->isGuest
                        )
                    ),
                    'htmlOptions' => array(
                        'pull' => BsHtml::NAVBAR_NAV_PULL_RIGHT
                    )
                )
            )
        ));
    }

    function MenuBar() {
        $this->widget('bootstrap.widgets.BsNavbar', array(
            'collapse' => true,
            'position' => BsHtml::NAVBAR_POSITION_FIXED_BOTTOM,
            'brandLabel' => BsHtml::icon(BsHtml::GLYPHICON_HOME),
            'brandUrl' => Yii::app()->homeUrl,
            'color' => BsHtml::NAVBAR_COLOR_INVERSE,
            'items' => array(
                array(
                    'class' => 'bootstrap.widgets.BsNav',
                    'type' => 'navbar',
                    'activateParents' => true,
                    'items' => array(
                        array(
                            'icon' => BsHtml::GLYPHICON_ENVELOPE,
                            'title' => '',
                            'url' => array(
                                '/sistema/contacto/create'
                            )
                        ),
                        array(
                            'icon' => BsHtml::GLYPHICON_USER,
                            'title' => '',
                            'url' => array(
                                '/sistema/usuario/index'
                            ),
                            'visible' => !Yii::app()->user->isGuest
                        ),
                        array(
                            'label' => 'Entrar',
                            'icon' => BsHtml::GLYPHICON_OFF,
                            'url' => array(
                                '/site/login'
                            ),
                            'pull' => BsHtml::NAVBAR_NAV_PULL_RIGHT,
                            'visible' => Yii::app()->user->isGuest
                        ),
                        array(
                            'icon' => BsHtml::GLYPHICON_REMOVE_CIRCLE,
                            'label' => 'Salir (' . Yii::app()->user->name . ')',
                            'pull' => BsHtml::NAVBAR_NAV_PULL_RIGHT,
                            'url' => array(
                                '/site/logout'
                            ),
                            'visible' => !Yii::app()->user->isGuest
                        )
                    ),
                    'htmlOptions' => array(
                        'pull' => BsHtml::NAVBAR_NAV_PULL_RIGHT
                    )
                )
            )
        ));
    }

    /**
     * Crear los subopciones de los menu
     * @author Giovanni Ariza <sistemas@giosyst3m.net>
     * @since 2014-06-17
     * @version 1.0.0
     * @param type $padre
     * @return type
     */
    private function MenuSubOpciones($padre) {
        //Obtener todos las categorias segun el padre recibido
        $SisMenuSub = SisMenu::model()->findAll('padre=:padre AND r_d_s=1  ORDER BY orden', array(':padre' => $padre));
        //Inicializar los items del submenu
        $itemsSub = array();
        //Ciclo para buscar las subopciones
        for ($index = 0; $index < count($SisMenuSub); $index++) {
            //Inicialiar la etiqueta
            $itemsSub[$index]['label'] = $SisMenuSub[$index]->nombre;
            //Inicializar la URL
            $itemsSub[$index]['url'] = array($SisMenuSub[$index]->link);
            //Inicializar el icono
            $itemsSub[$index]['icon'] = $SisMenuSub[$index]->icono;
//            $itemsSub[$index]['class']=
            //Recorsivo para otro sub-opción
            $itemsSub[$index]['items'] = $this->MenuSubOpciones($SisMenuSub[$index]->id);
        }
        //Retornar el arreglo con las opciones
        return $itemsSub;
    }

    public function formatoFechaHora($fechahora, $formato = 'd-m-Y h:i:s') {
        return date($formato, strtotime($fechahora));
    }

    /**
     * Despleiga la imagenes
     * @author Giovanni Ariza <sistemas@giosyst3m.net>
     * @since 2014-06-22
     * @version 1.0.0
     * @param string $nombre nombre del archivo
     * @param string $url Ruta donde se almaceno
     * @param int $ancho Ancho para la imagen predeterminado 100px
     * @param int $alto Alto para la imagen predeterminado 100px
     * @param boolean $tumbenails Si desea que este en Tumbenails
     * @return html <img> imagen
     */
    public function showImagen($nombre = "", $url = "", $tumbenails = true, $ancho = 100, $alto = 100) {
        if (!empty($nombre)) {
            if ($tumbenails) {
                return BsHtml::imageThumbnail(Yii::app()->getBaseUrl(TRUE) . Yii::app()->params[$url] . $nombre, $nombre, array("style" => "width:$ancho" . "px;height:$alto" . "px;"));
            } else {
                return BsHtml::imageResponsive(Yii::app()->getBaseUrl(TRUE) . Yii::app()->params[$url] . $nombre, $nombre, array("style" => "width:$ancho" . "px;height:$alto" . "px;"));
            }
        } else {
            return BsHtml::imageResponsive(Yii::app()->request->hostInfo . Yii::app()->theme->baseUrl . '/images/no_disponible.png', 'no_disponible.png', array("style" => "width:$ancho" . "px;height:$alto" . "px;"));
        }
    }

    /**
     * Agrega al inventario los productos 
     * @author Giovanni Ariza <sistemas@giosyst3m.net>
     * @since 2014-07-26
     * @version 1.0.0
     * @param int $id_product ID del producto
     * @param int $quantity Cantidad
     * @param int $id_inv_origin Procedencia de la solicitud
     * @param int $id_order Numero de identificación el origen
     * @param int $id_ordDetail ID detalle de donde proviene
     * @param int $id_stock ID del almacen
     * @param boolean $agregar TRUE = agregar; FALSE = Disminiur
     * @return type
     */
    public function addInventory($id_product = 0, $quantity = 0, $id_inv_origin = 0, $id_order = 0, $id_ordDetail = 0, $id_stock = 0, $agregar = TRUE, $motivo = '', $stock = TRUE) {
        try {
            //Instanaciar ProInventario para agregar nuevos registro
            $model = new Inventory();
            //Asignar id del producto
            $model->id_product = $id_product;
            //Asignar id del almacen
            $model->id_stock = empty($id_stock) ? Yii::app()->user->getState('PRODUCT')->PRODUCT_STOCK_DEFAULT : $id_stock;
            if ($agregar) {
                //Asignar el tipo a 1 es una entrada
                $model->type = 1;
                //Asignar la cantidad 
                $model->quantity = (int) $quantity;
            } else {
                //Asignar el tipo a 1 es una salida
                $model->type = 2;
                //Asignar la cantidad multiplicada por -1 para pasar el valor a negativo, ya que es una salida, e ir disminuyendo
                $model->quantity = (int) $quantity * -1;
            }
            //Asignar el orgine de dodne se produjo
            $model->id_inv_origin = $id_inv_origin;
            //Log
            $msg = "INVENTARIO " .
                    ($agregar ? Yii::t('app', 'IN') : Yii::t('app', 'OUT')) .
                    " Origen: $id_inv_origin " .
                    InvOrigin::model()->findByPk($id_inv_origin)->name .
                    " Producto: $id_product " . Product::model()->findByPk($id_product)->name .
                    " Cantidad: " . $model->quantity .
                    " Almacen: $model->id_stock  " .
                    Stock::model()->findByPk($model->id_stock)->name;
            if (!empty($id_order)) {
                //Asignar el id del pedido que esta en la sesión
                $model->id_order = $id_order;
                $msg .= "Numero: $id_order ";
            }
            if (!empty($id_ordDetail)) {
                //Asiganr el ID de referencia de detalle
                $model->id_ord_detail = $id_ordDetail;
                $msg .= "Detalle: $id_ordDetail";
            }
            if (!empty($motivo)) {
                $msg .= " Motivo: " . $motivo;
            }
            //Asiganr el motivo
            $model->comment = $msg;
            //Log
            Yii::log($msg, 'info', __METHOD__ . ":" . __LINE__);
            //Guardar el registro
            $r = $model->save();
            if ($stock) {
                Product::model()->updateByPk($id_product, [
                    'stock' => VInventory::model()->findByAttributes(
                            [
                                'id_product' => $id_product,
                                'id_stock' => empty($id_stock) ? Yii::app()->user->getState('PRODUCT')->PRODUCT_STOCK_DEFAULT : $id_stock
                    ])->quantity
                ]);
            }
            return $r;
        } catch (Exception $e) {
            Yii::app()->user->setFlash('danger', $e->getCode() . ' ' . $e->getMessage());
        }
    }

    /**
     * Consulta el valor de la cuenta 
     * @author Giovanni Ariza <sistemas@giosyst3m.net>
     * @since 2014-07-27
     * @version 1.0.0
     * @param int $id ID de la Habitación
     * @return Object Resultados
     */
    public function getTotalesCuentas($id, $cuenta = 0) {
        if ($cuenta == 0) {
            //Consultar la cantidad de Items en la Cuenta
            $model = VCuenta::model()->find('id_habitacion=:habitacion AND abierta=:valor', array(':habitacion' => $id, ':valor' => 1));
        } else {
            $model = VCuenta::model()->find('id_cuenta=:cuenta', array(':cuenta' => $cuenta));
        }
        //SI NO hay datos
        if (empty($model)) {
            //Asignar a 0 la cantidad de Items
            $data['cantidad'] = 0;
            //Asigna a 0 El total
            $data['total'] = 0;
            //Asignar el numero de habitacion
            $data['numero'] = VHabitacion::model()->find('id=:habitacion', array(':habitacion' => $id))->numero;
            //Asginar las undiades
            $data['unidades'] = 0;
            $data['totalSinFormato'] = 0;
            $data['id_cuenta'] = 0;
            $data['id_habitacion'] = $id;
            $data['descuento'] = 0;
            $data['pago'] = 0;
            $data['disponible'] = 0;
            $data['adicional'] = 0;
            $data['adicionalSinFormato'] = 0;
            $data['factura']['total'] = 0;
        } else {
            //Asignar la cantidad existente de Items en la cuenta
            $data['cantidad'] = number_format(VCuentaCerrar::model()->count('id_cuenta=:cuenta AND abierta=:valor', array(':cuenta' => $model->id_cuenta, ':valor' => 1)));
            //Asignar la sumatoria de los Itmes
            $criteria = new CDbCriteria;
            $criteria->select = 'SUM(total) as total';
            $criteria->addCondition('id_cuenta=' . $model->id_cuenta);
            $criteria->addCondition('abierta=1');
            $criteria->addCondition('r_d_s=1');
            $data['total'] = number_format(VCuentaCerrar::model()->find($criteria)->total, 2);
            $data['totalSinFormato'] = (int) VCuentaCerrar::model()->find($criteria)->total;
            //Asignar la sumatoria de los undiades
            $criteria->select = 'SUM(cantidad) as cantidad';
            $criteria->addCondition('id_cuenta=' . $model->id_cuenta);
            $criteria->addCondition('abierta=1');
            $data['unidades'] = number_format(VCuentaCerrar::model()->find($criteria)->cantidad);
            $data['id_cuenta'] = $model->id_cuenta;
            $data['numero'] = $model->numero;
            $data['id_habitacion'] = $model->id_habitacion;
            //Validar el descuento
            $criteria = new CDbCriteria;
            $criteria->select = 'SUM(valor) as valor';
            $criteria->addCondition('id_cuenta=' . $model->id_cuenta);
            $criteria->addCondition('r_d_s=1');
            $data['descuento'] = (int) CueDescuento::model()->find($criteria)->valor;
            //Validar el Adicionales
            $criteria = new CDbCriteria;
            $criteria->select = 'SUM(valor) as valor';
//            $criteria->addCondition('id_cuenta='.$model->id_cuenta);
//            $criteria->addCondition('r_d_s=1');
            $criteria->addColumnCondition(array('id_cuenta' => $model->id_cuenta, 'r_d_s' => 1));
            $data['adicional'] = number_format(CueAdicional::model()->find($criteria)->valor);
            $data['adicionalSinFormato'] = (int) CueAdicional::model()->find($criteria)->valor;

            //Validar el pagos
            $criteria = new CDbCriteria;
            $criteria->select = 'SUM(valor) as valor';
            $criteria->addCondition('id_cuenta=' . $model->id_cuenta);
            $criteria->addCondition('r_d_s=1');
            $data['pago'] = (int) Pago::model()->find($criteria)->valor;
            $data['disponible'] = (($data['totalSinFormato'] - ($data['descuento'] * -1)) - $data['pago']) + $data['adicionalSinFormato'];
            ////

            $consulta = CHtml::listData(VCuentaCerrarTotal::model()->findAllByAttributes(array('id_cuenta' => $model->id_cuenta)), 'factura', 'total');

            $data['factura'] = array_combine(array_keys($consulta), array_values($consulta));
            $data['factura']['total'] = array_sum($consulta);
        }
        return (Object) $data;
    }

    public function getCuentasTotalesResumen($id) {
        $model = VCuenta::model()->find('id_habitacion=:habitacion AND abierta=:valor', array(':habitacion' => $id, ':valor' => 1));
        if ($model) {
            $consulta = CHtml::listData(VCuentaCerrarTotal::model()->findAllByAttributes(array('id_cuenta' => $model->id_cuenta)), 'factura', 'total');
            $data['factura']['total'] = array_sum($consulta);
        } else {
            $data['factura']['total'] = 0;
        }

        return (Object) $data;
    }

    public function pillsCuentas($id) {
        $total = $this->getTotalesCuentas($id);
        return BsHtml::stackedPills(array(
                    array(
                        'label' => BsHtml::badge($total->numero, array(
                            'pull' => BsHtml::PULL_RIGHT
                        )) . Yii::t('app', 'Room'),
                        'active' => true,
                        'url' => Yii::app()->createAbsoluteUrl('producto/cuenta/getCuenta', array('id' => $id)),
                        'icon' => BsHtml::GLYPHICON_HEART_EMPTY
                    ),
                    array(
                        'label' => BsHtml::badge($total->cantidad, array(
                            'pull' => BsHtml::PULL_RIGHT
                        )) . Yii::t('app', 'Items'),
                        'url' => Yii::app()->createAbsoluteUrl('producto/cuenta/getCuenta', array('id' => $id)),
                        'active' => TRUE,
                        'icon' => BsHtml::GLYPHICON_LIST
                    ),
                    array(
                        'label' => BsHtml::badge($total->unidades, array(
                            'pull' => BsHtml::PULL_RIGHT
                        )) . Yii::t('app', 'Unidades'),
                        'url' => Yii::app()->createAbsoluteUrl('producto/cuenta/getCuenta', array('id' => $id)),
                        'active' => TRUE,
                        'icon' => BsHtml::GLYPHICON_CUTLERY
                    ),
                    array(
                        'label' => BsHtml::badge($total->factura['total'], array(
                            'pull' => BsHtml::PULL_RIGHT
                        )) . Yii::t('app', 'Total'),
                        'url' => Yii::app()->createAbsoluteUrl('producto/cuenta/getCuenta', array('id' => $id)),
                        'active' => TRUE,
                        'icon' => BsHtml::GLYPHICON_USD
                    )
        ));
    }

    /**
     * Consulta el valor de la cuentas de empleados
     * @author Giovanni Ariza <sistemas@giosyst3m.net>
     * @since 2014-07-27
     * @version 1.0.0
     * @param int $id ID del usuario
     * @return Object Resultados
     */
    public function getTotalesEmpleado($id, $cuenta = 0) {
        //Consultar la cantidad de Items en la Cuenta
//        $model = VCuenta::model()->find('id_habitacion=:habitacion AND abierta=:valor',array(':habitacion'=>$id,':valor'=>1));
        if ($cuenta == 0) {
            $model = VCuentaSisUsuario::model()->findByAttributes(array('abierta' => 1, 'id_sis_usuario' => $id));
        } else {
            $model = VCuentaSisUsuario::model()->findByAttributes(array('id_cuenta' => $cuenta));
        }
        //SI NO hay datos
        if (empty($model)) {
            //Asignar a 0 la cantidad de Items
            $data['cantidad'] = 0;
            //Asigna a 0 El total
            $data['total'] = 0;
            //Asignar el numero de habitacion
            $data['numero'] = VSisUsuario::model()->find('id=:id', array(':id' => $id))->nombre;
            //Asginar las undiades
            $data['unidades'] = 0;
            $data['disponible'] = 0;
            $data['pago'] = 0;
        } else {
            //Asignar la cantidad existente de Items en la cuenta
            $data['cantidad'] = number_format(VCuentaSisUsuario::model()->count('id_cuenta=:cuenta AND abierta=:valor', array(':cuenta' => $model->id_cuenta, ':valor' => 1)));
            //Asignar la sumatoria de los Itmes
            $criteria = new CDbCriteria;
            $criteria->select = 'SUM(total) as total';
            $criteria->addCondition('id_cuenta=' . $model->id_cuenta);
            $criteria->addCondition('abierta=1');
            $criteria->addCondition('r_d_s=1');
            $data['total'] = number_format(VCuentaSisUsuario::model()->find($criteria)->total, 2);
            $data['totalSinFormato'] = (int) VCuentaSisUsuario::model()->find($criteria)->total;
            $data['factura']['sub-total'] = $data['totalSinFormato'];
            //Asignar la sumatoria de los undiades
            $criteria->select = 'SUM(cantidad) as cantidad';
            $criteria->addCondition('id_cuenta=' . $model->id_cuenta);
            $criteria->addCondition('abierta=1');
            $criteria->addCondition('r_d_s=1');
            $data['unidades'] = number_format(VCuentaSisUsuario::model()->find($criteria)->cantidad);
            //Validar el descuento
            $criteria = new CDbCriteria;
            $criteria->select = 'SUM(valor) as valor';
            $criteria->addCondition('id_cuenta=' . $model->id_cuenta);
            $criteria->addCondition('r_d_s=1');
            $data['descuento'] = (int) CueDescuento::model()->find($criteria)->valor;
            $data['factura']['descuento'] = $data['descuento'];
            $data['numero'] = $model->nombre;
            $data['id_cuenta'] = $model->id_cuenta;
            $data['id_sis_usuario'] = $model->id_sis_usuario;

            //Validar el pagos
            $criteria = new CDbCriteria;
            $criteria->select = 'SUM(valor) as valor';
            $criteria->addCondition('id_cuenta=' . $model->id_cuenta);
            $criteria->addCondition('r_d_s=1');
            $data['pago'] = (int) Pago::model()->find($criteria)->valor;
            $data['factura']['pagos'] = $data['pago'];
            $data['disponible'] = ($data['totalSinFormato'] - ($data['descuento'] * -1)) - $data['pago'];

            $data['factura']['total'] = $data['disponible'] = ($data['totalSinFormato'] - ($data['descuento'] * -1)) - $data['pago'];
        }
        return (Object) $data;
    }

    /**
     * Crea el pills del empleado
     * @param int $id id del USuario (Empelado)
     * @return string
     */
    public function pillsEmpleado($id) {
        $total = $this->getTotalesEmpleado($id);
        return BsHtml::stackedPills(array(
                    array(
                        'label' => BsHtml::badge($total->numero, array(
                            'pull' => BsHtml::PULL_RIGHT
                        )),
                        'active' => true,
                        'url' => Yii::app()->createAbsoluteUrl('producto/CuentaEmpleado/getCuenta', array('id' => $id)),
                        'icon' => BsHtml::GLYPHICON_USER
                    ),
                    array(
                        'label' => BsHtml::badge($total->cantidad, array(
                            'pull' => BsHtml::PULL_RIGHT
                        )) . Yii::t('app', 'Items'),
                        'url' => Yii::app()->createAbsoluteUrl('producto/CuentaEmpleado/getCuenta', array('id' => $id)),
                        'active' => TRUE,
                        'icon' => BsHtml::GLYPHICON_LIST
                    ),
                    array(
                        'label' => BsHtml::badge($total->unidades, array(
                            'pull' => BsHtml::PULL_RIGHT
                        )) . Yii::t('app', 'Unidades'),
                        'url' => Yii::app()->createAbsoluteUrl('producto/CuentaEmpleado/getCuenta', array('id' => $id)),
                        'active' => TRUE,
                        'icon' => BsHtml::GLYPHICON_CUTLERY
                    ),
                    array(
                        'label' => BsHtml::badge($total->disponible, array(
                            'pull' => BsHtml::PULL_RIGHT
                        )) . Yii::t('app', 'Total'),
                        'url' => Yii::app()->createAbsoluteUrl('producto/CuentaEmpleado/getCuenta', array('id' => $id)),
                        'active' => TRUE,
                        'icon' => BsHtml::GLYPHICON_USD
                    )
        ));
    }

    /**
     * Obetner la Nombre del mes segun el ID recibido
     * @param int $id ID del mes
     * @return string Nombre del Mes
     */
    public function getMes($id) {
        switch ($id) {
            case 1:
                return Yii::t('app', 'January');
            case 2:
                return Yii::t('app', 'February');
            case 3:
                return Yii::t('app', 'March');
            case 4:
                return Yii::t('app', 'April');
            case 5:
                return Yii::t('app', 'May');
            case 6:
                return Yii::t('app', 'June');
            case 7:
                return Yii::t('app', 'July');
            case 8:
                return Yii::t('app', 'August');
            case 9:
                return Yii::t('app', 'September');
            case 10:
                return Yii::t('app', 'October');
            case 11:
                return Yii::t('app', 'November');
            case 12:
                return Yii::t('app', 'December');
            default:
                return 'Error';
        }
    }

    /**
     * Abotiene una arreglo del dia de la semana
     * @param boolean $lunes True comienza desde Lunes = 0 la semana de lo contrario comienza desde lunes
     * @return array Arreglo con las semanas, según el día de unicio
     */
    public function getWeekDays($lunes = true) {
        if ($lunes) {
            return array(
                0 => 'Lunes',
                1 => 'Martes',
                2 => 'Miércoles',
                3 => 'Jueves',
                4 => 'Viernes',
                5 => 'Sábado',
                6 => 'Domingo'
            );
        } else {
            return array(
                0 => 'Domingo',
                1 => 'Lunes',
                2 => 'Martes',
                3 => 'Miércoles',
                4 => 'Jueves',
                5 => 'Viernes',
                6 => 'Sábado',
            );
        }
    }

    /**
     * Obtien el día de la semana
     * @param int $id día de la semana
     * @param boolean $lunes True Lunes = 0
     * @return string el día del a semana segun el id
     */
    public function getWeekDay($id, $lunes = TRUE) {
        if ($lunes) {
            switch ($id) {
                case 0:
                    return Yii::t('app', 'Lunes');
                case 1:
                    return Yii::t('app', 'Martes');
                case 2:
                    return Yii::t('app', 'Miércoles');
                case 3:
                    return Yii::t('app', 'Jueves');
                case 4:
                    return Yii::t('app', 'Viernes');
                case 5:
                    return Yii::t('app', 'Sábado');
                case 6:
                    return Yii::t('app', 'Domingo');
            }
        } else {
            switch ($id) {
                case 0:
                    return Yii::t('app', 'Domingo');
                case 1:
                    return Yii::t('app', 'Lunes');
                case 2:
                    return Yii::t('app', 'Martes');
                case 3:
                    return Yii::t('app', 'Miércoles');
                case 4:
                    return Yii::t('app', 'Jueves');
                case 5:
                    return Yii::t('app', 'Viernes');
                case 6:
                    return Yii::t('app', 'Sábado');
            }
        }
    }

    /**
     * Agregar  Hijo(s) al Padre
     * @author Giovanni Ariza <sistemas@giosyst3m.net>
     * @since 2014-08-02
     * @version 1.0.0
     * @param string $parent Nombre del Padre
     * @param array $data Hijos a incluir
     * @return boolean TRUE OK FLASE ERROR
     */
    public function addChild($parent = null, $data = array()) {
        for ($index = 0; $index < count($data); $index++) {
            if (!Yii::app()->authManager->addItemChild($parent, $data[$index])) {
                return false;
            }
        }
        return true;
    }

    /**
     * Genera un contraseña aleatoria
     * @author Yii Framework Gruop Moderators, Post 1.526
     * @version 1.0.0
     * @date    2009-06-22
     * @param int $length Longitud de la contraseña
     * @return string la contraseña generada
     */
    public function getPasswordRandom($length = 10) {
        $chars = array_merge(range(0, 9), range('a', 'z'), range('A', 'Z'));
        shuffle($chars);
        return implode(array_slice($chars, 0, $length));
    }

    /**
     * Envios de email
     * @author Giovanni Ariza <sistemas@giosyst3m.net>
     * @since 2014-08-14
     * @version 1.0.0
     * @param string $from Quien envía
     * @param string $to Para quien
     * @param string $subject titulo
     * @param string $msg Mensaje
     * @param string $ok Messaje OK
     * @param string $error Mesaje Errado
     * @param string $cc Con copia
     * @param string $bc Con copia oculta
     * @return string Resutaldo OK o Errror
     */
    public function sendEmail($from, $to, $subject, $msg, $ok, $error, $cc = null, $bc = null) {
        $headers[] = "MIME-Version: 1.0";
        $headers[] = "Content-Type: text/html; charset=UTF-8";
        $subject = '=?UTF-8?B?' . base64_encode($subject) . '?=';

        $from = Yii::app()->params['emailSupport'];
        $headers[] = "From: {$from}";
        if (!empty($cc)) {
            $headers[] = "Cc: {$cc}";
        }
        if (Yii::app()->params['emailSupportPedidoBcc'] || Yii::app()->params['emailSupportUserBcc']) {
            $headers[] = "Bcc: {" . Yii::app()->params['emailSupport'] . "}";
            ;
        }
        $headers[] = "Reply-To: {$from}";
        $success = mail($to, $subject, $msg, implode("\r\n", $headers));
        if ($success) {
            Yii::app()->user->setFlash('info', $ok);
        } else {
            Yii::app()->user->setFlash('danger', $error . ' - ' . $success);
        }
    }

    public function getEmailFromOrde($id) {
        //$model = VOrderSumary::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->find()
    }

    public function getUuser($id) {
        return VSisUsuario::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->find('id=' . $id);
    }

    /**
     * Consulta cuales son los emails en base de datos, segun el perfil que se recibe
     * @version 1.0.0
     * @param mix $perfil String seprados por , o Array
     * @return string Los emaisl separados por comas Ej Giovanni Ariza <sistemas@giosyst3m.net>, Albero Quiroz <giosyst3m@gmail.com>
     */
    public function getEmailsListByPerfil($perfil) {
        if (!is_array($perfil)) {
            $perfil = explode(',', $perfil);
        }
        $criteria = new CDbCriteria();
        $criteria->addInCondition('id_auth_item', $perfil);
        $criteria->addColumnCondition([
            'r_d_s' => 1,
            'acess' => 1,
        ]);
        $emails = null;
        $model = VSisUsuario::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->findAll($criteria);
        if ($model) {
            foreach ($model as $value) {
                $email[] = trim($value->complete_name) . "<" . trim($value->email) . ">";
            }
            $emails = implode(',', $email);
        }
        return $emails;
    }

    public function sendEmail2($email, $view, $subject, $cc = NULL, $emailOK = null, $emailError = null, $data = array(), $from = null, $layout = 'mail') {
        $mail = new YiiMailer();
        $mail->setView($view);
        if (!empty($data)) {
            $mail->setData($data);
        }
        $mail->setLayout($layout);
        if (empty($from)) {
            $mail->setFrom(Yii::app()->params['emailSupport'], Yii::t('msg', 'EMAIL_SUPPORT'));
        } else {
            $mail->setFrom($from);
        }
        if (ENV != 'dev' && Yii::app()->user->getState('SMTP')->SMTP_ACTIVE == 'true') {
            $mail->isSMTP();
            $mail->host = Yii::app()->user->getState('SMTP')->SMTP_HOST;
            $mail->port = Yii::app()->user->getState('SMTP')->SMTP_PORT;
            $mail->Username = Yii::app()->user->getState('SMTP')->SMTP_USER_NAME;
            $mail->Password = Yii::app()->user->getState('SMTP')->SMTP_PASSWORD;
        }
        $mail->setTo($email);
        if (!empty($cc)) {
            $mail->setCc($cc);
        }
        if (Yii::app()->params['emailSupportPedidoBcc'] || Yii::app()->params['emailSupportUserBcc']) {
            $mail->setBcc(Yii::app()->params['emailSupport']);
        }
        $mail->setSubject($subject);
        if ($mail->send()) {
            Yii::app()->user->setFlash('info', $emailOK);
        } else {
            Yii::app()->user->setFlash('danger', $emailError . '-' . $mail->getError());
        }
    }

    /**
     * Actualiza los campos de Auth para un registro
     * @author Giovanni Ariza <sistemas@giosyst3m.net>
     * @since 2014-08-16
     * @version 1.0.0
     * @param Object $model el modelo a actualiza
     * @param string $motivo Si algun motivo para cual se elimina el registro
     * @return boolean Si la operación se ejecuto correctamente
     */
    public function delete($model, $motivo = NULL, $quantity = false) {
        try {
            if ($model->r_d_s == 1) {
                $model->r_d_s = 0;
                $model->r_d_d = date('Y-m-d H:i:s');
                $model->r_d_u = Yii::app()->user->id;
                $model->r_d_i = Yii::app()->request->userHostAddress;
                if (!empty($motivo)) {
                    $model->r_d_r = $motivo;
                }
                if ($quantity) {
                    $model->quantity = 0;
                }
            } else {
                $model->r_d_s = 1;
            }
            if ($model->update()) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $e) {
            Yii::app()->user->setFlash('danger', $e->getCode() . ' ' . $e->getMessage());
        }
    }

    /**
     * Actualiza los campos de Auth para varios registros
     * @author Giovanni Ariza <sistemas@giosyst3m.net>
     * @since 2014-08-16
     * @version 1.0.0
     * @param Object $model el modelo a actualiza
     * @return boolean Si la operación se ejecuto correctamente
     */
    public function deleteAll($model) {
        try {
            if ($model->updateAll(array(
                        'r_d_s' => 0,
                        'r_d_d' => date('Y-m-d H:i:s'),
                        'r_d_u' => Yii::app()->user->id,
                        'r_d_i' => Yii::app()->request->userHostAddress,
                    ))) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $e) {
            Yii::app()->user->setFlash('danger', $e->getCode() . ' ' . $e->getMessage());
        }
    }

    /**
     * Retonar llos emails que tiene el perfil de Despacho
     * @author Giovanni Ariza <sistemas@giosyst3m.net>
     * @since 2014-08-17
     * @version 1.0.0
     * @return array Todos los emails existentes
     */
    public function getEmailsDespacho() {
        try {
            $model = SisUsuario::model()->findAll('id IN ("' . Yii::app()->params['emailDespachoUserID'] . '")');
            foreach ($model as $value) {
                $email[] = $value->email;
            }
            return $email;
        } catch (Exception $e) {
            Yii::app()->user->setFlash('danger', $e->getCode() . ' ' . $e->getMessage());
        }
    }

    /**
     * Crearcion de cuentas
     * @param in $CajMovimientos el modelo de la consulta de $CajMovimeintos con datos
     * @param int $habitacion ID de la habitación
     * @param int $fechahora Fecha y hora de entrada defecto null
     * @return int el id de Cuenta creada
     */
    public function setCuenta($CajMovimientos, $habitacion, $fechaHora = null) {
        try {
            //Instaciar para crear un registro nuevo
            $Cuenta = new Cuenta();
            //Asignar la fecha y hora del generación
            $Cuenta->fecha = empty($fechaHora) ? date('Y-m-d H:i:s') : $this->formatoFechaHora($fechaHora, 'Y-m-d H:i:s');
            //Asignar la Caja
            $Cuenta->id_caj_movimientos = $CajMovimientos;

            //Asignar el ID de la habitación
            //                $Cuenta->id_habitacion = $habitacion;
            //Asignar el resultado de la creación
            if ($Cuenta->save()) {
//                $this->resp['resp'] = 1;
                //Asgianr la respusta obtenida
//                $this->resp['info'] = BsHtml::alert(BsHtml::ALERT_COLOR_SUCCESS, Yii::t('msg', 'CUENTA_SAVE_OK'));
                //Asignar en sessión el ID del pedido generado
                $idCuenta = $Cuenta->getPrimaryKey();
                Yii::log('CUENTA se ha creado: ' . $idCuenta . ' Habitacion/Empleado:' . $habitacion . ' User: ' . Yii::app()->user->id, 'info', __METHOD__ . ":" . __LINE__);
                return $idCuenta;
            } else {
//                $this->resp['resp'] = 0;
//                //Asginar la respuesta obtenida
//                $this->resp['info'] = BsHtml::alert(BsHtml::ALERT_COLOR_DANGER, Yii::t('msg', 'CUENTA_SAVE_ERROR'),array('{origen}'=>'Cuenta'));
                Yii::log('CUENTA error al intentar de crear cuenta Habitacion: ' . $habitacion . ' User: ' . Yii::app()->user->id, 'error', __METHOD__ . ":" . __LINE__);
                return 0;
            }
        } catch (Exception $e) {
            Yii::app()->user->setFlash('danger', $e->getCode() . ' ' . $e->getMessage());
        }
    }

    /**
     * Registra el mesonero que recibo/entrega la habitación
     * @param int $idCuenta ID de la cuenta 
     * @param int $idMesonero id del usuario
     * @param int $log 1=Recibe; 0= Entrega
     * @return  boolean estado de la operación
     */
    public function setMesonero($idCuenta = 0, $idMesonero = 0, $log = 1) {
        $model = new CuentaMesonero();
        $model->id_cuenta = $idCuenta;
        $model->id_sis_usuario = $idMesonero;
        $model->id_sis_hab_log_status = $log;
        if ($model->save()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Obtiene las tarifas de la habtiación segun la dia y la horas, que este vigente
     * @param in $id de la Habitación
     * @param boolean $adicioneles Por Defecto False, para traer los costos adicionales de la habitación activos
     * @return objecct con la consulta obtenida modelo
     */
    public function getHabitacionTarifa($id = 0, $adicioneles = false) {
        $criteria = new CDbCriteria();
        $criteria->addCondition('id_habitacion=' . $id);
        $criteria->addCondition('dia_desde <= weekday(now()) and dia_hasta >= weekday(now())');
//        $criteria->addCondition("date(CONCAT('0000-00-00 ',hora_desde))  <= date(CONCAT('0000-00-00 ',time(now()))) and date(CONCAT('0000-00-00 ',hora_hasta)) >= date(CONCAT('0000-00-00 ',time(now())))");
        $criteria->addCondition("hora_desde <= time(now()) AND hora_hasta >= time(now())");
        $criteria->addColumnCondition(array('tar_hab_horario_r_d_s' => 1, 'tar_hab_horario_r_d_s' => 1, 'tarifario_r_d_s' => 1, 'hab_concepto_r_d_s' => 1));
        if ($adicioneles) {
            $criteria->addNotInCondition('id_hab_concepto', Yii::app()->params['TARIFAS_HABITACION_PRINCIPALES']);
        } else {
            $criteria->addInCondition('id_hab_concepto', Yii::app()->params['TARIFAS_HABITACION_PRINCIPALES']);
        }
        return VTarifarioHabitacionHorario::model()->findAll($criteria);
    }

    /**
     * Agrega a la cuenta activa de la habitación las tarifas seleccionadas y registra en el log,
     * @param int $idCuenta id de la cuenta
     * @param int  $tarifa id de la tarifa
     * @param int $cantidad Cantidad agregar por defecto 1
     * @param boolean $multiple Defacotul FALSE para agregar mas de uno al mismo tiempo, si TRUE el parametro de $data es obligatorio
     * @param array $data Listado de datos agregar
     * @return boolean Resutlado de las inserciones
     */
    public function addCuentaHabitacionConceptos($idCuenta, $tarifa, $cantidad = 1, $multiple = false, $data = array()) {
        if ($multiple) {
            foreach ($data as $key => $value) {
                //Instaciar para un nuevo registro en CuentaTarifarioHabitacion
                $CuentaTarifarioHabitacion = new CuentaTarifarioHabitacion();
                //Asignar el id de la cuenta
                $CuentaTarifarioHabitacion->id_cuenta = $idCuenta;
                //Asignar el id de la tarifa
                $CuentaTarifarioHabitacion->id_tarifario_habitacion = $value;
                //ASignar la cantidad
                $CuentaTarifarioHabitacion->cantidad = $cantidad;
                //Asignar el valor de la tarifa
                $CuentaTarifarioHabitacion->valor = VTarifarioHabitacionHorario::model()->findByAttributes(array('id' => $tarifa))->valor;
                //Asignar la Caja Aperturada Activa
                $CuentaTarifarioHabitacion->id_caj_movimientos = $this->getCajaAbierta();
                //Guardar datos en CuentaTarifarioHabitacion
                if (!$CuentaTarifarioHabitacion->save()) {
                    return false;
                }
                Yii::log("CUENTA TARIFA se agrego Adicionales Cuenta: $idCuenta Tarifa $tarifa Cantidad: 1 Valor: " . $CuentaTarifarioHabitacion->valor . " User: " . Yii::app()->user->id, 'info', __METHOD__ . ":" . __LINE__);
            }
            return TRUE;
        } else {
            //Instaciar para un nuevo registro en CuentaTarifarioHabitacion
            $CuentaTarifarioHabitacion = new CuentaTarifarioHabitacion();
            //Asignar el id de la cuenta
            $CuentaTarifarioHabitacion->id_cuenta = $idCuenta;
            //Asignar el id de la tarifa
            $CuentaTarifarioHabitacion->id_tarifario_habitacion = $tarifa;
            //ASignar la cantidad
            $CuentaTarifarioHabitacion->cantidad = $cantidad;
            //Asignar el valor de la tarifa
            $CuentaTarifarioHabitacion->valor = VTarifarioHabitacionHorario::model()->findByAttributes(array('id' => $tarifa))->valor;
            //Asignar la Caja Aperturada Activa
            $CuentaTarifarioHabitacion->id_caj_movimientos = $this->getCajaAbierta();
            //Guardar datos en CuentaTarifarioHabitacion
            //Agregar los adicionales si existen
            if ($CuentaTarifarioHabitacion->save()) {
                return TRUE;
            } else {
                return FALSE;
            }
            Yii::log("CUENTA TARIFA se agrego Cuenta: $idCuenta Tarifa $tarifa Cantidad: 1 Valor: " . $CuentaTarifarioHabitacion->valor . " User: " . Yii::app()->user->id, 'info', __METHOD__ . ":" . __LINE__);
        }
    }

    /**
     * Crear los pedidos a paritr de un cuenta, si la cuenta tiene productos, esta lo genera,
     * El sitema valida que tenga nevera la habtiaición
     * @param tyintpe $idCuenta ID de la Cuenta
     * @return boolean TRUE = ok FALSE = error
     */
    public function setPedido($idCuenta = 0) {
        try {
            //Consultar el detalle de la cuenta, si tiene productos para el pedido
            $cueDetalle = CueDetalle::model()->findAllByAttributes(array('id_cuenta' => $idCuenta, 'r_d_s' => 1));
            //Consultar los datos de la cuenta y la habitación, si tiene nevera
            $cuenta = VCuenta::model()->findByAttributes(array('id_cuenta' => $idCuenta));
            //Si tiene nevera, y productos para el pedido
            if ($cuenta && $cuenta->nevera == 1 && $cueDetalle) {
                //Instanciar Pedidos para crear un nuevo registro
                $pedido = new Pedido();
                //Asignar la fecha de creación si nembargo esta internamente con r_c_d
                $pedido->fecha = date("Y-m-d H:i:s");
                //Asignar el ID de la habitación
                $pedido->id_habitacion = $cuenta->id_habitacion;
                //Asigar el estado de la Orden
                $pedido->id_sis_ped_status = 1; //En Orden
                //SI se guardó correctamente el pedido
                if ($pedido->save()) {
                    //Registra en el log todo OK
                    Yii::log('PEDIDO  Automatico pedido Nro: ' . $pedido->getPrimaryKey() . ' Habitacion: ' . $pedido->id_habitacion . ' - ' . $cuenta->numero . ' User: ' . Yii::app()->user->id, 'info', __METHOD__ . "." . __LINE__);
                    //Llamar para agrega los productos del pedido
                    $this->addPedidoDetalle($pedido->id, $cueDetalle);
                } else {
                    //REgistra en el Log errores presentados
                    Yii::log('Se Presentado error al crear un nuevo pedido User' . Yii::app()->user->id, 'error', __METHOD__ . ":" . __LINE__);
                    //Retorna en falso si no pudo crear el pedido
                    return false;
                }
            } else {
                //Retorna en fals que no tenia habitación y/o productos para el pedido
                return false;
            }
        } catch (Exception $e) {
            Yii::app()->user->setFlash('danger', $e->getCode() . '' . $e->getMessage());
        }
    }

    /**
     * Agrega los productos del pedido partiendo del pedi ogenrado en setPedido()
     * Una vez finalizado hace el envio al despacho
     * Dejando el pedido en estus 2 en Orden para continuar el proceso.
     * @param int $idPedido ID del pedido generado
     * @param array $cueDetalle los productos que se agregaran, 
     */
    public function addPedidoDetalle($idPedido = 0, $cueDetalle = array()) {
        try {
            //Recorer cada productos para agregar al pedido
            foreach ($cueDetalle as $data) {
                //Instanciar PedDetalle para crear un nuevo registro
                $pedDetalle = new PedDetalle();
                //Asiganr el ID del pedido asociado
                $pedDetalle->id_pedido = $idPedido;
                //Asignar el ID del producto desde el ID_Tarifario_Producto
                $pedDetalle->id_producto = TarifarioProducto::model()->findByPk($data->id_tarifario_producto)->id_producto;
                //Asignar la cantidad solicitada
                $pedDetalle->solicitada = $data->cantidad;
                //Si guardó correctamente el PedDetalle
                if ($pedDetalle->save()) {
                    //Registra en el LOG todo O.K.
                    Yii::log('PEDIDO Automático se agregado  un Producto: ' . $data->idTarifarioProducto->id_producto . ' Cantidad:' . $data->cantidad . ' PedDetalle: ' . $pedDetalle->id . ' Pedido: ' . $idPedido . ' User: ' . Yii::app()->user->id, 'info', __METHOD__ . ":" . __LINE__);
                } else {
                    //Registra que se presentó problemas al agregar.
                    Yii::log('PEDIDO Automático Se presento un Error al tratar de crear un en PedDetalle Producto: ' . $data->idTarifarioProducto->id_producto . ' Cantidad:' . $data->cantidad . ' Pedido: ' . $idPedido . ' User: ' . Yii::app()->user->id, 'error', __METHOD__ . ":" . __LINE__);
                }
            }
            //Cambiar el estado del Pedido a 2 en ORDEN
            if ($this->updateStatus(2, $idPedido)) {
                //Enviar email de notificación al despacho
                $this->actionemailOrden($idPedido, true);
            }
        } catch (Exception $e) {
            Yii::app()->user->setFlash('danger', $e->getCode() . '' . $e->getMessage());
        }
    }

    /**

     * @param int $id el ID de la orden
     * @return email Correo electronico
     */
    public function actionemailOrden($id = 0, $pedido = false) {
        if ($pedido) {
            $email = Pedido::model()->find('id=:id', array(':id' => $id))->rcu->email;
        } else {
            $email = $this->getEmailsDespacho();
        }
        $status = VPedido::model()->find('id=:id', array(':id' => $id))->status;
        $this->sendEmail($email, 'orden', Yii::t('msg', 'EMAIL_ORDEN', array('{numero}' => $id, '{status}' => $status)), 'EMAIL_ORDEN_OK', 'EMAIL_ORDEN_ERROR', array('id' => $id, 'status' => $status));
    }

    /**
     * Actualiza el estatus de la Orden
     * @author Giovanni Ariza <sistemas@giosyst3m.net>
     * @since 2014-07-10
     * @version 1.0.0
     * @param int $status el id del estatus para acutualizar
     * @return boolean True= se actualizó bien False=error
     */
    public function updateStatus($status = 0, $id = 0) {
        try {
            $model = Order::model()->updateByPk($id, array('id_status' => $status));
            if ($model) {
                Yii::app()->user->setFlash('success', Yii::t('json', 2010));
                return true;
            } else {
                $msg = 'Se presento un error al tratar de Cambiar el estado del Pedido Nro.' . $id . ' Estado: ' . $status . ' User:' . Yii::app()->user->id;
                Yii::app()->user->setFlash('danger', Yii::t('json', 5003));
                Yii::log($msg, 'error', __METHOD__ . ":" . __LINE__);
                return FALSE;
            }
        } catch (Exception $e) {
            Yii::app()->user->setFlash('danger', $e->getCode() . ' ' . $e->getMessage());
        }
    }

    function getReporteLista() {
        return array(
            1 => 'Fechas',
            2 => 'Semanas',
            3 => 'Meses',
            4 => 'Años'
        );
    }

    /**
     * Guarda la información en el Log
     * @author Giovanni Ariza <sistemas@gisyst3ms.com>
     * @param type $id ID de la Habitación
     * @param type $id_cliente ID del cliente
     * @param type $fechahora Fecha y hora para el Log
     * @param type $log ID del estatus del log
     */
    protected function saveHabLog($id, $id_cliente, $fechahora, $log) {
        try {
            $HabLog = new HabLog();
            $HabLog->id_cliente = empty($id_cliente) ? 1 : $id_cliente;
            $HabLog->id_habitacion = $id;
            $HabLog->id_sis_hab_log_status = $log;
            $HabLog->fechahora = $fechahora;
            if ($HabLog->save()) {
                return true;
            } else {
                return FALSE;
            }
        } catch (Exception $e) {
            Yii::app()->user->setFlash('danger', $e->getCode() . ' ' . $e->getMessage());
        }
    }

    /**
     * 
     * @return int ID de la caja apertura del Cajero
     */
    public function getCajaAbierta() {
        $model = CajMovimientos::model()->findByAttributes(array(
//            'id_sis_usuario'=>  Yii::app()->user->id,
            'cierre' => null
        ));
        if ($model) {
            return $model->id;
        } else {
            return 0;
        }
    }

    /**
     * Impresiones HELPS
     * @param type $param
     * @param type $die
     * @param type $Json
     */
    public function _print($param, $die = false, $Json = false) {
        if ($Json) {
            echo CJSON::encode($param);
        } else {
            echo CHtml::openTag('pre');
            var_dump($param);
            echo CHtml::closeTag('pre');
        }
        if ($die) {
            Yii::app()->end();
        }
    }

    /**
     * Create a web friendly URL slug from a string.
     * 
     * Although supported, transliteration is discouraged because
     *     1) most web browsers support UTF-8 characters in URLs
     *     2) transliteration causes a loss of information
     *
     * @author Sean Murphy <sean@iamseanmurphy.com>
     * @copyright Copyright 2012 Sean Murphy. All rights reserved.
     * @license http://creativecommons.org/publicdomain/zero/1.0/
     *
     * @param string $str
     * @param array $options
     * @return string
     */
    function url_slug($str, $options = array()) {
        // Make sure string is in UTF-8 and strip invalid UTF-8 characters
        $str = mb_convert_encoding((string) $str, 'UTF-8', mb_list_encodings());

        $defaults = array(
            'delimiter' => '-',
            'limit' => null,
            'lowercase' => true,
            'replacements' => array(),
            'transliterate' => false,
        );

        // Merge options
        $options = array_merge($defaults, $options);

        $char_map = array(
            // Latin
            'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A', 'Å' => 'A', 'Æ' => 'AE', 'Ç' => 'C',
            'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I',
            'Ð' => 'D', 'Ñ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ö' => 'O', 'Ő' => 'O',
            'Ø' => 'O', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'U', 'Ű' => 'U', 'Ý' => 'Y', 'Þ' => 'TH',
            'ß' => 'ss',
            'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a', 'å' => 'a', 'æ' => 'ae', 'ç' => 'c',
            'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i',
            'ð' => 'd', 'ñ' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'o', 'ő' => 'o',
            'ø' => 'o', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ü' => 'u', 'ű' => 'u', 'ý' => 'y', 'þ' => 'th',
            'ÿ' => 'y',
            // Latin symbols
            '©' => '(c)',
            // Greek
            'Α' => 'A', 'Β' => 'B', 'Γ' => 'G', 'Δ' => 'D', 'Ε' => 'E', 'Ζ' => 'Z', 'Η' => 'H', 'Θ' => '8',
            'Ι' => 'I', 'Κ' => 'K', 'Λ' => 'L', 'Μ' => 'M', 'Ν' => 'N', 'Ξ' => '3', 'Ο' => 'O', 'Π' => 'P',
            'Ρ' => 'R', 'Σ' => 'S', 'Τ' => 'T', 'Υ' => 'Y', 'Φ' => 'F', 'Χ' => 'X', 'Ψ' => 'PS', 'Ω' => 'W',
            'Ά' => 'A', 'Έ' => 'E', 'Ί' => 'I', 'Ό' => 'O', 'Ύ' => 'Y', 'Ή' => 'H', 'Ώ' => 'W', 'Ϊ' => 'I',
            'Ϋ' => 'Y',
            'α' => 'a', 'β' => 'b', 'γ' => 'g', 'δ' => 'd', 'ε' => 'e', 'ζ' => 'z', 'η' => 'h', 'θ' => '8',
            'ι' => 'i', 'κ' => 'k', 'λ' => 'l', 'μ' => 'm', 'ν' => 'n', 'ξ' => '3', 'ο' => 'o', 'π' => 'p',
            'ρ' => 'r', 'σ' => 's', 'τ' => 't', 'υ' => 'y', 'φ' => 'f', 'χ' => 'x', 'ψ' => 'ps', 'ω' => 'w',
            'ά' => 'a', 'έ' => 'e', 'ί' => 'i', 'ό' => 'o', 'ύ' => 'y', 'ή' => 'h', 'ώ' => 'w', 'ς' => 's',
            'ϊ' => 'i', 'ΰ' => 'y', 'ϋ' => 'y', 'ΐ' => 'i',
            // Turkish
            'Ş' => 'S', 'İ' => 'I', 'Ç' => 'C', 'Ü' => 'U', 'Ö' => 'O', 'Ğ' => 'G',
            'ş' => 's', 'ı' => 'i', 'ç' => 'c', 'ü' => 'u', 'ö' => 'o', 'ğ' => 'g',
            // Russian
            'А' => 'A', 'Б' => 'B', 'В' => 'V', 'Г' => 'G', 'Д' => 'D', 'Е' => 'E', 'Ё' => 'Yo', 'Ж' => 'Zh',
            'З' => 'Z', 'И' => 'I', 'Й' => 'J', 'К' => 'K', 'Л' => 'L', 'М' => 'M', 'Н' => 'N', 'О' => 'O',
            'П' => 'P', 'Р' => 'R', 'С' => 'S', 'Т' => 'T', 'У' => 'U', 'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C',
            'Ч' => 'Ch', 'Ш' => 'Sh', 'Щ' => 'Sh', 'Ъ' => '', 'Ы' => 'Y', 'Ь' => '', 'Э' => 'E', 'Ю' => 'Yu',
            'Я' => 'Ya',
            'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'yo', 'ж' => 'zh',
            'з' => 'z', 'и' => 'i', 'й' => 'j', 'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o',
            'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c',
            'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sh', 'ъ' => '', 'ы' => 'y', 'ь' => '', 'э' => 'e', 'ю' => 'yu',
            'я' => 'ya',
            // Ukrainian
            'Є' => 'Ye', 'І' => 'I', 'Ї' => 'Yi', 'Ґ' => 'G',
            'є' => 'ye', 'і' => 'i', 'ї' => 'yi', 'ґ' => 'g',
            // Czech
            'Č' => 'C', 'Ď' => 'D', 'Ě' => 'E', 'Ň' => 'N', 'Ř' => 'R', 'Š' => 'S', 'Ť' => 'T', 'Ů' => 'U',
            'Ž' => 'Z',
            'č' => 'c', 'ď' => 'd', 'ě' => 'e', 'ň' => 'n', 'ř' => 'r', 'š' => 's', 'ť' => 't', 'ů' => 'u',
            'ž' => 'z',
            // Polish
            'Ą' => 'A', 'Ć' => 'C', 'Ę' => 'e', 'Ł' => 'L', 'Ń' => 'N', 'Ó' => 'o', 'Ś' => 'S', 'Ź' => 'Z',
            'Ż' => 'Z',
            'ą' => 'a', 'ć' => 'c', 'ę' => 'e', 'ł' => 'l', 'ń' => 'n', 'ó' => 'o', 'ś' => 's', 'ź' => 'z',
            'ż' => 'z',
            // Latvian
            'Ā' => 'A', 'Č' => 'C', 'Ē' => 'E', 'Ģ' => 'G', 'Ī' => 'i', 'Ķ' => 'k', 'Ļ' => 'L', 'Ņ' => 'N',
            'Š' => 'S', 'Ū' => 'u', 'Ž' => 'Z',
            'ā' => 'a', 'č' => 'c', 'ē' => 'e', 'ģ' => 'g', 'ī' => 'i', 'ķ' => 'k', 'ļ' => 'l', 'ņ' => 'n',
            'š' => 's', 'ū' => 'u', 'ž' => 'z'
        );

        // Make custom replacements
        $str = preg_replace(array_keys($options['replacements']), $options['replacements'], $str);

        // Transliterate characters to ASCII
        if ($options['transliterate']) {
            $str = str_replace(array_keys($char_map), $char_map, $str);
        }

        // Replace non-alphanumeric characters with our delimiter
        $str = preg_replace('/[^\p{L}\p{Nd}]+/u', $options['delimiter'], $str);

        // Remove duplicate delimiters
        $str = preg_replace('/(' . preg_quote($options['delimiter'], '/') . '){2,}/', '$1', $str);

        // Truncate slug to max. characters
        $str = mb_substr($str, 0, ($options['limit'] ? $options['limit'] : mb_strlen($str, 'UTF-8')), 'UTF-8');

        // Remove delimiter from ends
        $str = trim($str, $options['delimiter']);

        return $options['lowercase'] ? mb_strtolower($str, 'UTF-8') : $str;
    }

    /**
     * Generator to Form addition fileds
     * @param model $model Model
     * @param array $car value
     */
    function render_filelds($model, $car = null, $url = Null) {
        $htmlOut = '';
        foreach ($model as $value) {

            $required = ($value->require == 1 ? BsHtml::tag('span', array('class' => 'required'), ' *', true) : '');
            $requiredClass = ($value->require == 1 ? 'required' : '');
            switch ($value->id_fie_type) {
                case 1://String
                    $htmlOut .= BsHtml::textFieldControlGroup('car[' . $value->id . ']', isset($car[$value->id]) ? $car[$value->id] : '', array(
                                'help' => $value->description,
                                'placeholder' => $value->description,
                                'label' => $value->label . $required,
                                'class' => $requiredClass
                    ));
                    break;
                case 2://TextArea
                    $htmlOut .= BsHtml::textAreaControlGroup('car[' . $value->id . ']', isset($car[$value->id]) ? $car[$value->id] : '', array(
                                'help' => $value->description,
                                'placeholder' => $value->description,
                                'label' => $value->label, 'car[' . $value->id . ']' . $required,
                                'class' => $requiredClass
                    ));
                    break;
                case 3://Number
                    $htmlOut .= BsHtml::numberFieldControlGroup('car[' . $value->id . ']', isset($car[$value->id]) ? $car[$value->id] : '', array(
                                'help' => $value->description,
                                'placeholder' => $value->description,
                                'label' => $value->label . $required,
                                'class' => 'text-right ' . $requiredClass,
                    ));
                    break;
                case 4://DateTime
                    $htmlOut .= BsHtml::dateFieldControlGroup('car[' . $value->id . ']', isset($car[$value->id]) ? $car[$value->id] : '', array(
                                'help' => $value->description,
                                'placeholder' => $value->description,
                                'label' => $value->label . $required,
                                'class' => 'datetimepicker ' . $requiredClass,
                                'append' => BsHtml::icon(BsHtml::GLYPHICON_CALENDAR),
                    ));
                    break;
                case 5://Date
                    $htmlOut .= BsHtml::dateFieldControlGroup('car[' . $value->id . ']', isset($car[$value->id]) ? $car[$value->id] : '', array(
                                'help' => $value->description,
                                'placeholder' => $value->description,
                                'label' => $value->label . $required,
                                'class' => 'date ' . $requiredClass,
                                'append' => BsHtml::icon(BsHtml::GLYPHICON_CALENDAR),
                    ));
                    break;
                case 6://Time
                    $htmlOut .= BsHtml::textFieldControlGroup('car[' . $value->id . ']', isset($car[$value->id]) ? $car[$value->id] : '', array(
                                'help' => $value->description,
                                'placeholder' => $value->description,
                                'label' => $value->label . $required,
                                'class' => 'timepicker ' . $requiredClass,
                                'append' => BsHtml::icon(BsHtml::GLYPHICON_TIME),
                    ));
                    break;
                case 7://img
                    $htmlOut .= BsHtml::fileFieldControlGroup('car[' . $value->id . ']', '', array(
                                'help' => $value->description,
                                'placeholder' => $value->description,
                                'label' => $value->label . $required,
                                'class' => $requiredClass
                    ));
                    if (isset($car[$value->id])) {
                        $htmlOut .= '<div class="center-block text-center">';
                        $htmlOut .= $this->ShowImagen($car[$value->id], $url);
                        $htmlOut .= '</div><div class="clearfix">&nbsp;</div>';
                    }
                    break;
                case 8://DropDown One Select
                    $htmlOut .= BsHtml::dropDownListControlGroup('car[' . $value->id . ']', isset($car[$value->id]) ? $car[$value->id] : '', array_combine(explode(',', $value->value), explode(',', $value->value)), array(
                                'help' => $value->description,
                                'placeholder' => $value->description,
                                'label' => $value->label . $required,
                                'data-style' => "btn-primary",
                                'data-live-search' => true,
                                'class' => 'selectpicker show-tick ' . $requiredClass,
                                'empty' => Yii::t('app', '.::Select::.'),
                    ));
                    break;
                case 9://DropDown Multiple Select
                    $htmlOut .= BsHtml::dropDownListControlGroup('car[' . $value->id . ']', isset($car[$value->id]) ? explode(',', $car[$value->id]) : array(), array_combine(explode(',', $value->value), explode(',', $value->value)), array(
                                'help' => $value->description,
                                'placeholder' => $value->description,
                                'label' => $value->label . $required,
                                'multiple' => 'multiple',
                                'class' => 'selectpicker show-tick ' . $requiredClass,
                                'data-live-search' => true,
                                'title' => Yii::t('app', '.::Select::.'),
                                'data-style' => "btn-primary"
                    ));
                    break;
                case 10://Radio
                    $htmlOut .= BsHtml::radioButtonListControlGroup('car[' . $value->id . ']', isset($car[$value->id]) ? $car[$value->id] : '', array_combine(explode(',', $value->value), explode(',', $value->value)), array(
                                'help' => $value->description,
                                'placeholder' => $value->description,
                                'label' => $value->label . $required,
                                'class' => $requiredClass
                    ));
                    break;
                case 11://Check
                    $htmlOut .= BsHtml::checkBoxListControlGroup('car[' . $value->id . ']', isset($car[$value->id]) ? explode(',', $car[$value->id]) : array(), array_combine(explode(',', $value->value), explode(',', $value->value)), array(
                                'help' => $value->description,
                                'placeholder' => $value->description,
                                'label' => $value->label . $required,
                                'class' => $requiredClass
                    ));
                    break;
                case 12://Link
                    $htmlOut .= BsHtml::urlFieldControlGroup('car[' . $value->id . ']', isset($car[$value->id]) ? $car[$value->id] : '', array(
                                'help' => $value->description,
                                'placeholder' => $value->description,
                                'label' => $value->label . $required,
                                'class' => $requiredClass
                    ));
                    if (isset($car[$value->id])) {
                        $htmlOut .= BsHtml::link($car[$value->id], $car[$value->id], array(
                                    'icon' => BsHtml::GLYPHICON_LINK,
                                    'target' => '_blanck'
                        ));
                    }
                    break;
                case 13://File
                    $htmlOut .= BsHtml::fileFieldControlGroup('car[' . $value->id . ']', '', array(
                                'help' => $value->description,
                                'placeholder' => $value->description,
                                'label' => $value->label . $required,
                                'class' => $requiredClass
                    ));
                    if (isset($car[$value->id])) {
                        $htmlOut .= BsHtml::link($car[$value->id], Yii::app()->getBaseUrl(true) . Yii::app()->params[$url] . $car[$value->id], array(
                                    'icon' => BsHtml::GLYPHICON_FILE
                        ));
                    }
                    break;
                default:
                    break;
            }
        }
        return $htmlOut;
    }

    public function random_color_part() {
        return str_pad(dechex(mt_rand(0, 255)), 2, '0', STR_PAD_LEFT);
    }

    public function random_color() {
        return '#' . $this->random_color_part() . $this->random_color_part() . $this->random_color_part();
    }

    public function getPeriodos() {
        return array(
            1 => 'Anual',
            2 => 'Semestral',
            3 => 'Cuatrimestral',
            4 => 'Trimestral',
            5 => 'Bimestral',
            6 => 'Mensual',
        );
    }

    public function getSemestres() {
        return array(
            "Semestre I" => 'Semestre 1 (Ene, Feb, Mar, Abr, May, Jun)',
            "Semestre II" => 'Semestre 2 (Jul, Ago, Sep, Oct, Nov, Dic)',
        );
    }

    public function getCuatrimestres() {
        return array(
            "Cuatrimestre I" => 'Cuatrimestre 1 (Ene, Feb, Mar, Abr)',
            "Cuatrimestre II" => 'Cuatriemstre 2 (May, Jun, Jul, Ago)',
            "Cuatrimestre III" => 'Cuatriemstre 3 (Sep, Oct, Nov, Dic)',
        );
    }

    public function getTrimestres() {
        return array(
            "Trimestre I" => 'Trimestre 1 (Ene, Feb, Mar)',
            "Trimestre II" => 'Trimestre 2 (Abr, May, Jun)',
            "Trimestre III" => 'Trimestre 3 (Jul, Ago, Sep)',
            "Trimestre IV" => 'Trimestre 4 (Oct, Nov, Dic)',
        );
    }

    public function getBimestres() {
        return array(
            "Bimestre I" => 'Bimestre 1 (Ene, Feb)',
            "Bimestre II" => 'Bimestre 2 (Mar, Abr)',
            "Bimestre III" => 'Bimestre 3 (May, Jun)',
            "Bimestre IV" => 'Bimestre 4 (Jul, Ago)',
            "Bimestre V" => 'Bimestre 4 (Sep, Oct)',
            "Bimestre VI" => 'Bimestre 4 (Nov, Dic)',
        );
    }

    public function getMonth() {
        return array(
            "Enero" => 'Enero',
            "Febrero" => 'Febrero',
            "Marzo" => 'Marzo',
            "Abril" => 'Abril',
            "Mayo" => 'Mayo',
            "Junio" => 'Junio',
            "Julio" => 'Julio',
            "Agosto" => 'Agosto',
            "Septiembre" => 'Septiembre',
            "Octubre" => 'Octubre',
            "Noviembre" => 'Noviembre',
            "Diciembre" => 'Diciembre',
        );
    }

    public function getCharts() {
        return array(
            'PieChart' => 'Torta',
            'LineChart' => 'Lineal',
            'BarChart' => 'Barras',
            'ColumnChart' => 'Columnas',
        );
    }

    public function clearString($value = array()) {
        return '"' . implode('","', $value) . '"';
    }

    public function createCounters($id_company) {
        $model = Document::model()->findAll('r_d_s=1');
        foreach ($model as $value) {
            $SysCounter = new SysCounter();
            $SysCounter->id_company = $id_company;
            $SysCounter->id_document = $value->id;
            $SysCounter->save();
        }
    }

    public function getNumber($id_document = 0) {
        $SysCounter = SysCounter::model()->findByAttributes(array(
                    'id_company' => Yii::app()->user->getState('id_company'),
                    'id_document' => $id_document,
                    'r_d_s' => 1
                ))->number;
        $number = $SysCounter + 1;
        SysCounter::model()->updateAll(array(
            'number' => $number,
                ), 'id_company=:id_company and id_document=:id_document and r_d_s=1', array(
            ':id_company' => Yii::app()->user->getState('id_company'),
            ':id_document' => $id_document
        ));
        return $number;
    }

    public function getCompanySisUser() {
        return BsHtml::listData(VCompanySisUsuario::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->findAllByAttributes(array('id_sis_usuario' => Yii::app()->user->id)), 'id_company', 'company');
    }

    public function changeCompany($id, $print = true) {
        Yii::app()->user->setState('id_company', $id);
        Yii::app()->user->setState('id_stock', (int) VStockCompany::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->find('id_company=:id and id_sto_type=1', array(':id' => $id))->id_stock);
        if ($print) {
            echo $this->getJSon(200, Yii::t('json', 2016, array('{company}' => Company::model()->cache(Yii::app()->user->getState('CACHE')->CACHE_REFRESH_TIME)->findByPk($id)->name)));
        }
    }

    /**
     * Agrega Mensajes a la orden de manera automática
     * @author Giovanni Ariza <sistemas@giosyst3m.net>
     * @since 1.0.0
     * @version 1.0.0
     * @param int $id ID del Pedido
     * @param text $message Mensaje que se esta agregando
     * @param int $priority Prioridad 1=Bajo; 2= Normal; 3=Alta; 4=Urgente; Por defecto 1
     * @return boolean True = todo ok, False: error
     */
    public function addCommentToOrder($id, $message, $priority = 1) {
        $model = new Comment();
        $model->message = $message;
        $model->id_com_priority = $priority;
        if ($model->save()) {
            $order = new CommentOrder();
            $order->id_order = $id;
            $order->id_comment = $model->getPrimaryKey();
            if ($order->save()) {
                return true;
            }
        }
        return false;
    }

    public function getLinktypeFile($type, $url, $html = []) {
        return BsHtml::Link(BsHtml::tag('span', ['class' => $this->getIconTypeFile($type)]), $url, $html);
    }

    public function getIconTypeFile($type) {
        switch ($type) {
            case 'image/png':
            case 'image/jpeg':
                return 'fa fa-file-image-o fa-2x';
                break;
            case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
                return 'fa fa-file-word-o fa-2x';
                break;
            case 'application/pdf':
                return 'fa fa-file-pdf-o fa-2x';
                break;
            default:
                return 'fa fa-file fa-2x';
                break;
        }
    }

    public function saveLog($data, $category) {
        $model = new Log();
        $model->message = CJSON::encode($data);
        $model->row = $data->id;
        $model->category = $category;
        $model->save();
    }
    
    

}
