<?php
/* @var $this UserController */
/* @var $dataProvider CActiveDataProvider */
?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/script/user/index.js', CClientScript::POS_END); ?>
<?php
$this->menu = array(
    array('icon' => 'glyphicon glyphicon-plus-sign', 'label' => 'Create User', 'url' => array('create')),
    array('icon' => 'glyphicon glyphicon-tasks', 'label' => 'Manage User', 'url' => array('admin')),
);
?>

<div class="x_panel">
    <div class="x_title">
        <h2>Account Information</h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <!-- Smart Wizard -->
        <div id="wizard" class="form_wizard wizard_horizontal">
            <ul class="wizard_steps">
                <li>
                    <a href="#step-1">
                        <span class="step_no">1</span>
                        <span class="step_descr">
                            Step 1<br />
                            <small>Personal</small>
                        </span>
                    </a>
                </li>
                <li>
                    <a href="#step-2">
                        <span class="step_no">2</span>
                        <span class="step_descr">
                            Step 2<br />
                            <small>Address</small>
                        </span>
                    </a>
                </li>
                <li>
                    <a href="#step-3">
                        <span class="step_no">3</span>
                        <span class="step_descr">
                            Step 3<br />
                            <small>Payment</small>
                        </span>
                    </a>
                </li>
                <li>
                    <a href="#step-4">
                        <span class="step_no">4</span>
                        <span class="step_descr">
                            Step 4<br />
                            <small>Success</small>
                        </span>
                    </a>
                </li>
            </ul>
            <div id="step-1">
                <h3>Personal Information</h3>
                <?php $this->renderPartial('_form_personal', array('model' => $model_personal)); ?>
            </div>
            <div id="step-2">
                <h3>Address Information</h3>
                <?php $this->renderPartial('_form_address', array('model' => $model_address)); ?>
            </div>
            <div id="step-3">
                <h3>Account Information</h3>
                <?php $this->renderPartial('_form_account', array('model' => $model_account)); ?>
            </div>
            <div id="step-4">
                <h3 id="error_user">Payment Information</h3>
                <div  id="success" class="alert alert-success alert-dismissible fade in" role="alert">
                </div>
                <button id="succes_buttom" onclick="lastStep(true)" class="btn btn-primary">Thank you, click here to finish</button>
                <div  id="danger" class="alert alert-danger alert-dismissible fade in" role="alert"  class="text-center" style="display: none">
                  
                    Account Owner: <span id="account_user"></span><br>
                    IBAN: <span id="iban_user"></span><br>
                    Please Try again and verify your information <button onclick="lastStep(false)" class="btn btn-primary">Click here</button> and update them
                    
                </div>
                <br>
                <br>
                <br>
                <br>
                <br>
            </div>
        </div>
        <!-- End SmartWizard Content -->
    </div>
</div>
<input id="_id" type="hidden">