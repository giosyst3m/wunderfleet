<?php
/* @var $this UserController */
/* @var $data User */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
		<?php echo CHtml::encode($data->name); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('lastname')); ?>:</b>
		<?php echo CHtml::encode($data->lastname); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('phone')); ?>:</b>
		<?php echo CHtml::encode($data->phone); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('step')); ?>:</b>
		<?php echo CHtml::encode($data->step); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('address')); ?>:</b>
		<?php echo CHtml::encode($data->address); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('additional')); ?>:</b>
		<?php echo CHtml::encode($data->additional); ?>
		<br />

	<?php if(Yii::app()->user->checkAccess('UserViewAuthView')){?>
		<b><?php echo CHtml::encode($data->getAttributeLabel('city')); ?>:</b>
		<?php echo CHtml::encode($data->city); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('payment')); ?>:</b>
		<?php echo CHtml::encode($data->payment); ?>
		<br />

		<b><?php echo CHtml::encode($data->getAttributeLabel('r_c_d')); ?>:</b>
		<?php echo CHtml::encode($data->r_c_d); ?>
		<br />

	<?php }?>

</div>