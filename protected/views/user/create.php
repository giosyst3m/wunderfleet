<?php
/* @var $this UserController */
/* @var $model User */
?>

<?php
$this->breadcrumbs = array(
    'Users' => array('create'),
    'Create',
);
?>

<?php echo BsHtml::pageHeader(Yii::t('app', 'Create'), Yii::t('app', 'User')) ?>

<?php
$this->renderPartial('_form', array('model' => $model));
?>
<hr>
<?php
$this->renderPartial('admin', array('model2' => $model2));
?>