<?php

/* @var $this UserController */
/* @var $model User */
?>

<?php

$this->breadcrumbs = array(
    'Users' => array('index'),
    $model->name,
);
?>

<?php echo BsHtml::pageHeader('View', 'User ' . $model->id) ?>
<?php

$reg = array(
    'id',
    'name',
    'lastname',
    'phone',
    'step',
    'address',
    'additional',
    'city',
    'payment',
    'r_c_d',
    array(
        'name' => 'r_d_s',
        'type' => 'raw',
        'value' => $model->RegistroEstado($model->r_d_s)
    ),
);
?><?php

$this->widget('zii.widgets.CDetailView', array(
    'htmlOptions' => array(
        'class' => 'table table-striped table-condensed table-hover',
    ),
    'data' => $model,
    'attributes' => $reg
        ,
));
?>

<?php

echo BsHtml::button(Yii::t('app', 'Back'), array(
    'name' => 'btnBack',
    'class' => 'uibutton loading confirm',
    'color' => BsHtml::BUTTON_COLOR_PRIMARY,
    'onclick' => 'history.go(-1)',
        )
);
echo BsHtml::link(Yii::t('app', 'New'), Yii::app()->createAbsoluteUrl('User/create'), array('class' => 'btn btn-primary'));
?>