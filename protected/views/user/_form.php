<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form BSActiveForm */
?>

<?php
$form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id' => 'user-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation' => true,
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    )
        ));
?>

<p class="help-block"> <?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required.') ?></p>

<?php echo $form->errorSummary($model); ?>

<?php echo $form->textFieldControlGroup($model, 'id'); ?>
<?php echo $form->textFieldControlGroup($model, 'name', array('maxlength' => 50)); ?>
<?php echo $form->textFieldControlGroup($model, 'lastname', array('maxlength' => 50)); ?>
<?php echo $form->textFieldControlGroup($model, 'phone', array('maxlength' => 15)); ?>
<?php echo $form->textFieldControlGroup($model, 'step'); ?>
<?php echo $form->textFieldControlGroup($model, 'address', array('maxlength' => 25)); ?>
<?php echo $form->textFieldControlGroup($model, 'additional', array('maxlength' => 15)); ?>
<?php echo $form->textFieldControlGroup($model, 'city', array('maxlength' => 50)); ?>
<?php echo $form->textFieldControlGroup($model, 'payment', array('maxlength' => 500)); ?>
<?php echo $form->textFieldControlGroup($model, 'r_c_d'); ?>

<?php
echo $form->dropDownListControlGroup($model, 'r_d_s', array(1 => 'Activo', 0 => 'Inactivo'), array('data-style' => 'btn-info', 'class' => 'selectpicker show-tick'));
?> 
<?php
echo BsHtml::submitButton(yii::t('app', 'Submit'), array('color' => BsHtml::BUTTON_COLOR_PRIMARY));
?>    
<?php
echo BsHtml::link(Yii::t('app', 'New'), Yii::app()->createAbsoluteUrl('User/create'), array('class' => 'btn btn-primary'));
?>  
<?php $this->endWidget(); ?>
