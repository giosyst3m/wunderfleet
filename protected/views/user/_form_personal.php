<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form BSActiveForm */
?>

<?php
$form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id' => 'user-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation' => true,
    'layout' => BsHtml::FORM_LAYOUT_VERTICAL,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    )
        ));
?>

<?php echo $form->errorSummary($model); ?>


<?php echo $form->textFieldControlGroup($model, 'name', array('maxlength' => 50)); ?>
<?php echo $form->textFieldControlGroup($model, 'lastname', array('maxlength' => 50)); ?>
<?php echo $form->textFieldControlGroup($model, 'phone', array('maxlength' => 15)); ?>
<?php // echo BsHtml::submitButton('Save', array('color' => BsHtml::BUTTON_COLOR_PRIMARY));?>    
<?php $this->endWidget(); ?>
