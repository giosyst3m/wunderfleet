<?php
/* @var $this UserController */
/* @var $model User */
?>

<?php
$this->breadcrumbs = array(
    'Users' => array('create'),
    $model->name => array('view', 'id' => $model->id),
    'Update',
);
?>

<?php echo BsHtml::pageHeader(Yii::t('app', 'Update'), Yii::t('app', 'User') . ' Nro: ' . $model->id) ?>

<?php
$this->renderPartial('_form', array('model' => $model));
?>
<hr>
<?php
$this->renderPartial('admin', array('model2' => $model2));
?>