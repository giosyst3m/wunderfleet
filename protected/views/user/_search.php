<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
    'layout' => BsHtml::FORM_LAYOUT_SEARCH,
)); ?>

    <?php echo $form->textFieldControlGroup($model,'id'); ?>
    <?php echo $form->textFieldControlGroup($model,'name',array('maxlength'=>50)); ?>
    <?php echo $form->textFieldControlGroup($model,'lastname',array('maxlength'=>50)); ?>
    <?php echo $form->textFieldControlGroup($model,'phone',array('maxlength'=>15)); ?>
    <?php echo $form->textFieldControlGroup($model,'step'); ?>
    <?php echo $form->textFieldControlGroup($model,'address',array('maxlength'=>25)); ?>
    <?php echo $form->textFieldControlGroup($model,'additional',array('maxlength'=>15)); ?>
    <?php echo $form->textFieldControlGroup($model,'city',array('maxlength'=>50)); ?>
    <?php echo $form->textFieldControlGroup($model,'payment',array('maxlength'=>500)); ?>
    <?php echo $form->textFieldControlGroup($model,'r_c_d'); ?>

    <div class="form-actions">
        <?php echo BsHtml::submitButton(Yii::t('app','Search'),  array('color' => BsHtml::BUTTON_COLOR_PRIMARY,));?>
    </div>

<?php $this->endWidget(); ?>
