<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form BSActiveForm */
?>

<?php
$form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id' => 'user-form-account',
    'action' => '/index.php/user/validAccount',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation' => true,
    'layout' => BsHtml::FORM_LAYOUT_VERTICAL,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    )
        ));
?>

<?php echo $form->errorSummary($model); ?>


<?php echo $form->textFieldControlGroup($model, 'account', array('maxlength' => 25)); ?>
<?php echo $form->textFieldControlGroup($model, 'iban', array('maxlength' => 15)); ?>
<?php $this->endWidget(); ?>
