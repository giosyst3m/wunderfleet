<?php

class UserController extends Controller {

    private $SEED = '@USER@';

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column1';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' 
                'actions' => array('index', 'validPersonal', 'validAddress', 'validAccount', 'getData'),
            ),
            array('allow', // allow all users to perform 'view' 
                'actions' => array('view'),
            ),
            array('allow', // allow all users to perform 'create' 
                'actions' => array('create'),
            ),
            array('allow', // allow all users to perform 'update' 
                'actions' => array('update'),
            ),
            array('allow', // allow all users to perform 'admin' 
                'actions' => array('admin'),
            ),
            array('allow', // allow all users to perform 'delete' 
                'actions' => array('delete'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new User;
        $model2 = new User('search');
        $model2->unsetAttributes();  // clear any default values
        if (isset($_GET['User']))
            $model2->attributes = $_GET['User'];

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['User'])) {
            try {
                $model->attributes = $_POST['User'];
                if ($model->save()) {
                    Yii::app()->user->setFlash('success', Yii::t('app', 'SUCCESS_CREATE_OK', array('{info}' => Yii::t('app', 'User'))));
                    $this->redirect(array('create', 'id' => $model->id));
                } else {
                    Yii::app()->user->setFlash('danger', Yii::t('app', 'SUCCESS_CREATE_ERROR', array('{info}' => Yii::t('app', 'User'))));
                }
            } catch (Exception $e) {
                Yii::app()->user->setFlash('danger', $e->getCode() . ' ' . $e->getMessage());
            }
        }

        $this->render('create', array(
            'model' => $model, 'model2' => $model2
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);
        $model2 = new User('search');
        $model2->unsetAttributes();  // clear any default values
        if (isset($_GET['User']))
            $model2->attributes = $_GET['User'];

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['User'])) {
            try {
                $model->attributes = $_POST['User'];
                if ($model->save()) {
                    Yii::app()->user->setFlash('success', Yii::t('app', 'SUCCESS_UPDATE_OK', array('{info}' => Yii::t('app', 'User'))));
                    $this->redirect(array('create', 'id' => $model->id));
                } else {
                    Yii::app()->user->setFlash('danger', Yii::t('app', 'SUCCESS_UPDATE_ERROR', array('{info}' => Yii::t('app', 'User'))));
                }
            } catch (Exception $e) {
                Yii::app()->user->setFlash('danger', $e->getCode() . ' ' . $e->getMessage());
            }
        }

        $this->render('update', array(
            'model' => $model, 'model2' => $model2
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {

        if (Yii::app()->request->isPostRequest) {
            // we only allow deletion via POST request
            try {
                $this->delete($this->loadModel($id));
            } catch (Exception $e) {
                throw new CHttpException(500, Yii::t('msg', 'DELETE_ERROR', array('{info}' => Yii::t('app', 'User'))));
            }
            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }else {
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $model = new User('personal');
        $model_address = new User('address');
        $model_account = new User('account');

        $this->performAjaxValidation($model);

        $this->render('index', array(
            'model_personal' => $model,
            'model_address' => $model_address,
            'model_account' => $model_account,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new User('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['User']))
            $model->attributes = $_GET['User'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return User the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = User::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param User $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'user-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'user-form-address') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'user-form-account') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * Valida Personal Information
     * if is New Create
     * if is old Update.
     * @param String $id
     * @return JSON Valid each field
     */
    public function actionvalidPersonal($id = null) {
        if (empty($id)) {
            $model = new User('personal');
        } else {
            $model = User::model()->find('_id = :id', [':id' => $id]);
            $model->scenario = 'personal';
        }
        $this->performAjaxValidation($model);

        if (isset($_POST['User'])) {
            $model->attributes = $_POST['User'];
            $model->step = 1;
            if ($model->save()) {
                $model->updateByPk($model->id, ['_id' => md5($this->SEED . $model->id)]);
                $response = $this->getJSon(200, 'Personal Information has been saved', ['_id' => md5($this->SEED . $model->id), 'step' => 1]);
            } else {
                $response = $this->getJSon(400, 'Some information is required', $model->errors);
            }
        }
        echo $response;
    }

    /**
     * Valid Addres User from ID
     * @param string $id
     * @return JSON Valid each field
     */
    public function actionvalidAddress($id = NULL) {
        if (empty($id)) {
            $model = new User('address');
        } else {
            $model = User::model()->find('_id = :id', [':id' => $id]);
            $model->scenario = 'address';
        }

        $this->performAjaxValidation($model);

        if (isset($_POST['User'])) {
            $model->attributes = $_POST['User'];
            $model->step = 2;
            if ($model->save()) {
                $response = $this->getJSon(200, 'Address Information has been saved', ['_id' => md5($this->SEED . $model->id), 'step' => 2]);
            } else {
                $response = $this->getJSon(400, 'Some information is required', $model->errors);
            }
        }
        echo $response;
    }

    /**
     * Valid a Account information from ID
     * @param string $id 
     * @return JSON Valid each field
     */
    public function actionvalidAccount($id = NULL) {
        if (empty($id)) {
            $model = new User('account');
        } else {
            $model = User::model()->find('_id = :id', [':id' => $id]);
            $model->scenario = 'account';
        }

        $this->performAjaxValidation($model);

        if (isset($_POST['User'])) {
            $model->attributes = $_POST['User'];
            $model->step = 3;
            $payment = $this->getPayment($model);
            if(isset($payment->paymentDataId)){
                $model->payment = $payment->paymentDataId;
            }
            if ($model->save()) {

                $response = $this->getJSon(200, 'Account Information has been saved', ['payment' => $payment, '_id' => md5($this->SEED . $model->id), 'step' => 3]);
            } else {
                $response = $this->getJSon(400, 'Some information is required', $model->errors);
            }
        }
        echo $response;
    }

    /**
     * Get Data from ID user
     * @param string $id
     * @return JSON Full data show en Front End.
     */
    public function actiongetData($id) {
        $criteria = new CDbCriteria;
        $criteria->select = 't.name, t.lastname, t.phone, t.address, t.additional, t.zip, t.city, t.account, t.additional, t.iban, t.step'; // select fields which you want in output
        $criteria->condition = 't._id = "' . $id . '"';

        $model = User::model()->find($criteria);
        // $model = User::model()->find('_id = :id', [':id' => $id])->findColumn(['name']);
        if ($model->save()) {
            $response = $this->getJSon(200, 'Information has been recoveried', $model);
        } else {
            $response = $this->getJSon(400, 'Some information is no complete', $model->errors);
        }
        echo $response;
    }

    /**
     * Get Payment ID Data
     * @param object $model
     * @return JSON Paymen from External API
     */
    private function getPayment($model) {
        //API URL
        $url = 'https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data';

        //create a new cURL resource
        $ch = curl_init($url);

        //setup request to send json via POST
        $data = array(
            'customerId' => $model->id,
            'iban' => $model->iban,
            'owner' => $model->account
        );
        $payload = json_encode($data);

        //attach encoded JSON string to the POST fields
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);

        //set the content type to application/json
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

        //return response instead of outputting
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        //execute the POST request
        $result = curl_exec($ch);
        $result = json_decode($result);
        $result->status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $result->status_desc = $this->getHttpStatus(curl_getinfo($ch, CURLINFO_HTTP_CODE));
        //close cURL resource
        curl_close($ch);
        return $result;
    }

    /**
     * Return Description http status
     * @param int $code
     * @return string
     * @author Giosyst3m
     */
    private function getHttpStatus($code) {
        $status = [
        200 => 'OK',
        201 => 'Create',
        202 => 'Accepted',
        203 => 'Non-Authoritative Information',
        204 => 'No Content',
        205 => 'Reset Content',
        206 => 'Partial Content',
        400 => 'Bad Request',
        401 => 'Unathorizad..',
        402 => 'Payment Required',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Method Not Allowed',
        406 => 'Not Acceptable',
        407 => 'Proxy Authentication Required',
        408 => 'Request Timeout',
        409 => 'Conflict',
        410 => 'Gone',
        411 => 'Lengh Required',
        412 => 'Precondition Failed',
        413 => 'Reqeust Entity Too Large',
        414 => 'Unsupported Media Type',
        415 => 'Request Range Not Satisfiable',
        416 => 'Expectation Failed',
        ];
        
        return $status[$code];
    }

}
