<?php
define('SERVER', $_SERVER['SERVER_NAME']);

switch (SERVER) {
    case 'wunder.local':
        // specify how many levels of call stack should be shown in each log message
        defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);
        // remove the following lines when in production mode
        defined('YII_DEBUG') or define('YII_DEBUG', true);
        define('ENV', 'dev');
        break;
    case 'pedidos-dev.giosyst3m.net':
    default:
        // specify how many levels of call stack should be shown in each log message
        defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 1);
        // remove the following lines when in production mode
        defined('YII_DEBUG') or define('YII_DEBUG', false);
        define('ENV', 'liv');
        break;
}
// change the following paths if necessary
$yii = dirname(__FILE__) . '/yii/framework/yii.php';
$config = dirname(__FILE__) . '/protected/config/main.php';





require_once($yii);
Yii::createWebApplication($config)->run();
